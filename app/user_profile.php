<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_profile extends Model
{
    //
     protected $table = 'user_profile_o';
      protected $primaryKey = 'USER_ID';
    public $incrementing = false;
    protected $keyType = String;
    public $timestamps = false;
}
