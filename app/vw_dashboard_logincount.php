<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vw_dashboard_logincount extends Model
{
    //
    protected $table = 'vw_dashboard_loginsum';
    protected $primaryKey = 'DATES';
    public $incrementing = false;
    protected $keyType = String;
    public $timestamps = false;

}
