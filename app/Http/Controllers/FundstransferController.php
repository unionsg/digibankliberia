<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Requester;

use Datatables;
use DB;
use Request;
use Session;
use App\User;

class FundstransferController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    /**
     * Display a listing of the mbank ach trans.
     *
     * @return \Illuminate\Http\Response
     */
     public function mbank_intertrans()
    {
        //
        $today = strtoupper(date('Y-m-d'));
        $currency = DB::table('tb_currency')
                        ->get();

        // echo $currency; die;
         $mbank_trans = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BL_TRA'],
                                    ['POSTING_DATE', '>=', $today]
                                    ])
                            ->orderBy('posting_date','desc')
                            ->get();

        $usd_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BL_TRA'],
                                    ['CURRENCY', '=', 'USD'],
                                    ['POSTING_DATE', '>=', $today]
                                    ])
                            ->count();

        $eur_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BL_TRA'],
                                    ['CURRENCY', '=', 'EUR'],
                                    ['POSTING_DATE', '>=', $today]
                                    ])
                            ->count();

        $gbp_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BL_TRA'],
                                    ['CURRENCY', '=', 'GBP'],
                                    ['POSTING_DATE', '>=', $today]
                                    ])
                            ->count();

         $sll_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BL_TRA'],
                                    ['CURRENCY', '=', 'SLL'],
                                    ['POSTING_DATE', '>=', $today]
                                    ])
                            ->count();

         $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['USD', 'EUR','GBP','SLL'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'hoverBackgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'data' => [$usd_count, $eur_count,$gbp_count,$sll_count]
                        ]
                    ])
                    ->options([]);

        
         return view('ftrans.mbank.itrans',['mbank_intratrans' => $mbank_trans,
                                            'chartjs' => $chartjs,
                                            'currencys' => $currency]);
    }
    /**
     * Display a listing of the ibank internal trans.
     *
     * @return \Illuminate\Http\Response
     */
     public function ibank_intertrans()
    {
        //
        $today = strtoupper(date('Y-m-d'));
        $currency = DB::table('tb_currency')
                        ->get();

        // echo $currency; die;
         $ibank_trans = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['POSTING_DATE', '>=', $today],
                                    ['TYPE_OF_ACCT','I']
                                    ])
                            ->orderBy('posting_date','desc')
                            ->get();

        $usd_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'USD'],
                                    ['POSTING_DATE', '>=', $today],
                                    ['TYPE_OF_ACCT','I']
                                    ])
                            ->count();

        $eur_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'EUR'],
                                    ['POSTING_DATE', '>=', $today],
                                    ['TYPE_OF_ACCT','I']
                                    ])
                            ->count();

        $gbp_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'GBP'],
                                    ['POSTING_DATE', '>=', $today],
                                    ['TYPE_OF_ACCT','I']
                                    ])
                            ->count();

         $sll_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'SLL'],
                                    ['POSTING_DATE', '>=', $today],
                                    ['TYPE_OF_ACCT','I']
                                    ])
                            ->count();

         $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['USD', 'EUR','GBP','SLL'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'hoverBackgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'data' => [$usd_count, $eur_count,$gbp_count,$sll_count]
                        ]
                    ])
                    ->options([]);

        
         return view('ftrans.ibank.itrans',['ibank_intratrans' => $ibank_trans,
                                            'chartjs' => $chartjs,
                                            'currencys' => $currency]);
    }
     /**
     * Display a listing of the mbank internal transfer search.
     *
     * @return \Illuminate\Http\Response
     */
     public function mbank_intertranssearch()
    {
        
         $inputs = Request::all();
        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $curr = $inputs['currency'];


        $currency = DB::table('tb_currency')
                        ->get();

        // echo $currency; die;
         $mbank_trans = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BL_TRA'],
                                    ['currency','like','%'.$curr.'%']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->orderBy('posting_date','desc')
                            ->get();

        $usd_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BL_TRA'],
                                    ['CURRENCY', '=', 'USD']
                                    ])
                            ->count();

        $eur_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BL_TRA'],
                                    ['CURRENCY', '=', 'EUR']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

        $gbp_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BL_TRA'],
                                    ['CURRENCY', '=', 'GBP']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

         $sll_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BL_TRA'],
                                    ['CURRENCY', '=', 'SLL']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

         $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['USD', 'EUR','GBP','SLL'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'hoverBackgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'data' => [$usd_count, $eur_count,$gbp_count,$sll_count]
                        ]
                    ])
                    ->options([]);

        
         return view('ftrans.mbank.itrans',['mbank_intratrans' => $mbank_trans,
                                            'chartjs' => $chartjs,
                                            'currencys' => $currency]);
    }
    
    /**
     * Display a listing of the ibank internal transfer search.
     *
     * @return \Illuminate\Http\Response
     */
     public function ibank_intertranssearch()
    {
        
         $inputs = Request::all();
        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $curr = $inputs['currency'];


        $currency = DB::table('tb_currency')
                        ->get();

        // echo $currency; die;
         $ibank_trans = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['currency','like','%'.$curr.'%'],
                                    ['TYPE_OF_ACCT','I']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->orderBy('posting_date','desc')
                            ->get();

        $usd_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'USD'],
                                    ['TYPE_OF_ACCT','I']
                                    ])
                            ->count();

        $eur_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'EUR'],
                                    ['TYPE_OF_ACCT','I']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

        $gbp_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'GBP'],
                                    ['TYPE_OF_ACCT','I']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

         $sll_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'SLL'],
                                    ['TYPE_OF_ACCT','I']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

         $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['USD', 'EUR','GBP','SLL'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'hoverBackgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'data' => [$usd_count, $eur_count,$gbp_count,$sll_count]
                        ]
                    ])
                    ->options([]);

        
         return view('ftrans.ibank.itrans',['ibank_intratrans' => $ibank_trans,
                                            'chartjs' => $chartjs,
                                            'currencys' => $currency]);
    }

     /**
     * Display a listing of the cbank internal trans.
     *
     * @return \Illuminate\Http\Response
     */
     public function cbank_intertrans()
    {
        //
        $today = strtoupper(date('Y-m-d'));
        $currency = DB::table('tb_currency')
                        ->get();

        // echo $currency; die;
         $ibank_trans = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['POSTING_DATE', '>=', $today],
                                    ['TYPE_OF_ACCT','C']
                                    ])
                            ->orderBy('posting_date','desc')
                            ->get();

        $usd_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'USD'],
                                    ['POSTING_DATE', '>=', $today],
                                    ['TYPE_OF_ACCT','C']
                                    ])
                            ->count();

        $eur_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'EUR'],
                                    ['POSTING_DATE', '>=', $today],
                                    ['TYPE_OF_ACCT','C']
                                    ])
                            ->count();

        $gbp_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'GBP'],
                                    ['POSTING_DATE', '>=', $today],
                                    ['TYPE_OF_ACCT','C']
                                    ])
                            ->count();

         $sll_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'SLL'],
                                    ['POSTING_DATE', '>=', $today],
                                    ['TYPE_OF_ACCT','C']
                                    ])
                            ->count();

         $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['USD', 'EUR','GBP','SLL'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'hoverBackgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'data' => [$usd_count, $eur_count,$gbp_count,$sll_count]
                        ]
                    ])
                    ->options([]);

        
         return view('ftrans.ibank.ctrans',['ibank_intratrans' => $ibank_trans,
                                            'chartjs' => $chartjs,
                                            'currencys' => $currency]);
    }

     /**
     * Display a listing of the cbank internal transfer search.
     *
     * @return \Illuminate\Http\Response
     */
     public function cbank_intertranssearch()
    {
        
         $inputs = Request::all();
        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $curr = $inputs['currency'];


        $currency = DB::table('tb_currency')
                        ->get();

        // echo $currency; die;
         $ibank_trans = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['currency','like','%'.$curr.'%'],
                                    ['TYPE_OF_ACCT','C']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->orderBy('posting_date','desc')
                            ->get();

        $usd_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'USD'],
                                    ['TYPE_OF_ACCT','C']
                                    ])
                            ->count();

        $eur_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'EUR'],
                                    ['TYPE_OF_ACCT','C']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

        $gbp_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'GBP'],
                                    ['TYPE_OF_ACCT','C']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

         $sll_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'NET'],
                                    ['VOUCHER_NUMBER', '=', 'INTB'],
                                    ['CURRENCY', '=', 'SLL'],
                                    ['TYPE_OF_ACCT','C']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

         $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['USD', 'EUR','GBP','SLL'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'hoverBackgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'data' => [$usd_count, $eur_count,$gbp_count,$sll_count]
                        ]
                    ])
                    ->options([]);

        
         return view('ftrans.ibank.ctrans',['ibank_intratrans' => $ibank_trans,
                                            'chartjs' => $chartjs,
                                            'currencys' => $currency]);
    }

     /**
     * Display a listing of the mbank ach trans.
     *
     * @return \Illuminate\Http\Response
     */
     public function mbank_qrtrans()
    {
        //
        $today = strtoupper(date('Y-m-d'));
         $currency = DB::table('tb_currency')
                        ->get();

        // echo $currency; die;
         $mbank_trans = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BLQR_TRA'],
                                    ['POSTING_DATE','>=',$today]
                                    ])
                            ->orderBy('posting_date','desc')
                            ->get();

        $usd_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BLQR_TRA'],
                                    ['CURRENCY', '=', 'USD'],
                                    ['POSTING_DATE','>=',$today]
                                    ])
                            ->count();

        $eur_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BLQR_TRA'],
                                    ['CURRENCY', '=', 'EUR'],
                                    ['POSTING_DATE','>=',$today]
                                    ])
                            ->count();

        $gbp_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BLQR_TRA'],
                                    ['CURRENCY', '=', 'GBP'],
                                    ['POSTING_DATE','>=',$today]
                                    ])
                            ->count();

         $sll_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BLQR_TRA'],
                                    ['CURRENCY', '=', 'SLL'],
                                    ['POSTING_DATE','>=',$today]
                                    ])
                            ->count();

         $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['USD', 'EUR','GBP','SLL'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'hoverBackgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'data' => [$usd_count, $eur_count,$gbp_count,$sll_count]
                        ]
                    ])
                    ->options([]);

        
         return view('ftrans.mbank.qrtrans',['mbank_qrtrans' => $mbank_trans,
                                            'chartjs' => $chartjs,
                                            'currencys' => $currency]);
    }
     /**
     * Display a listing of the mbank internal transfer search.
     *
     * @return \Illuminate\Http\Response
     */
     public function mbank_qrsearch()
    {
        
         $inputs = Request::all();
        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $curr = $inputs['currency'];


        $currency = DB::table('tb_currency')
                        ->get();

        // echo $currency; die;
         $mbank_trans = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BLQR_TRA'],
                                    ['currency','like','%'.$curr.'%']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->orderBy('posting_date','desc')
                            ->get();

        $usd_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BLQR_TRA'],
                                    ['CURRENCY', '=', 'USD']
                                    ])
                            ->count();

        $eur_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BLQR_TRA'],
                                    ['CURRENCY', '=', 'EUR']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

        $gbp_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BLQR_TRA'],
                                    ['CURRENCY', '=', 'GBP']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

         $sll_count = DB::table('VW_DASHBOARD_TRANS')
                             ->where([
                                    ['channel', '=', 'MOB'],
                                    ['VOUCHER_NUMBER', '=', 'BLQR_TRA'],
                                    ['CURRENCY', '=', 'SLL']
                                    ])
                             ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

         $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['USD', 'EUR','GBP','SLL'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'hoverBackgroundColor' => ['#FF6384', '#36A2EB','#e3f30c','#05b904'],
                            'data' => [$usd_count, $eur_count,$gbp_count,$sll_count]
                        ]
                    ])
                    ->options([]);

        
         return view('ftrans.mbank.qrtrans',['mbank_qrtrans' => $mbank_trans,
                                            'chartjs' => $chartjs,
                                            'currencys' => $currency]);
    }
     /**
     * Display a listing of the mbank salone link trans.
     *
     * @return \Illuminate\Http\Response
     */
     public function mbank_slinktrans()
    {
        //
         $today = strtoupper(date('Y-m-d'));

         $mbank_trans = DB::table('VW_DASHBOARD_SLINK')
                            ->where([
                                ['channel','MOB'],
                                ['posting_date','>=',$today]
                            ])
                            ->orderBy('posting_date','desc')
                            ->get();

          $redeem_count = DB::table('VW_DASHBOARD_SLINK')
                            ->where([
                                ['channel','MOB'],
                                ['flag','P'],
                                ['posting_date','>=',$today]
                            ])
                            ->count();
          $unredeem_count = DB::table('VW_DASHBOARD_SLINK')
                            ->where([
                                ['channel','MOB'],
                                ['flag','A'],
                                ['posting_date','>=',$today]
                            ])
                            ->count();


          $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['Settled', 'Not Settled'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#FF6384', '#36A2EB'],
                            'hoverBackgroundColor' => ['#029b17', '#36A2EB'],
                            'data' => [$redeem_count, $unredeem_count]
                        ]
                    ])
                    ->options([]);

         return view('ftrans.mbank.slink',['mbank_slinktrans' => $mbank_trans,
                                            'chartjs' => $chartjs]);
    }

     /**
     * Display a listing of the Ibank salone link trans.
     *
     * @return \Illuminate\Http\Response
     */
     public function ibank_slinktrans()
    {
        //
         $today = strtoupper(date('Y-m-d'));

         $ibank_trans = DB::table('VW_DASHBOARD_SLINK')
                            ->where([
                                ['channel','NET'],
                                ['posting_date','>=',$today]
                             ])
                            ->orderBy('posting_date','desc')
                            ->get();

          $redeem_count = DB::table('VW_DASHBOARD_SLINK')
                            ->where([
                                ['channel','NET'],
                                ['flag','P'],
                                ['posting_date','>=',$today]
                            ])
                            ->count();
          $unredeem_count = DB::table('VW_DASHBOARD_SLINK')
                            ->where([
                                ['channel','NET'],
                                ['flag','A'],
                                ['posting_date','>=',$today]
                            ])
                            ->count();


          $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['Settled', 'Not Settled'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#FF6384', '#36A2EB'],
                            'hoverBackgroundColor' => ['#029b17', '#36A2EB'],
                            'data' => [$redeem_count, $unredeem_count]
                        ]
                    ])
                    ->options([]);

         return view('ftrans.ibank.slink',['ibank_slinktrans' => $ibank_trans,
                                            'chartjs' => $chartjs]);
    }

    
     /**
     * Display a listing of the mbank salone link trans by search parameters.
     *
     * @return \Illuminate\Http\Response
     */
     public function mbank_slinksearch()
    {
        //
        $inputs = Request::all();
        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $status = $inputs['status'];

         $mbank_trans = DB::table('VW_DASHBOARD_SLINK')
                            ->where([
                                ['channel','MBANK'],
                                ['flag','like','%'.$status.'%']
                                ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->orderBy('posting_date','desc')
                            ->get();

          $redeem_count = DB::table('VW_DASHBOARD_SLINK')
                            ->where([
                                ['channel','MBANK'],
                                ['flag','P']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();
          $unredeem_count = DB::table('VW_DASHBOARD_SLINK')
                            ->where([
                                ['channel','MBANK'],
                                ['flag','A']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();


          $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['Settled', 'Not Settled'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#FF6384', '#36A2EB'],
                            'hoverBackgroundColor' => ['#029b17', '#36A2EB'],
                            'data' => [$redeem_count, $unredeem_count]
                        ]
                    ])
                    ->options([]);

         return view('ftrans.mbank.slink',['mbank_slinktrans' => $mbank_trans,
                                            'chartjs' => $chartjs]);
    }
     /**
     * Display a listing of the ibank salone link trans by search parameters.
     *
     * @return \Illuminate\Http\Response
     */
     public function ibank_slinksearch()
    {
        //
        $inputs = Request::all();
        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $status = $inputs['status'];

         $ibank_trans = DB::table('VW_DASHBOARD_SLINK')
                            ->where([
                                ['channel','NET'],
                                ['flag','like','%'.$status.'%']
                                ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->orderBy('posting_date','desc')
                            ->get();

          $redeem_count = DB::table('VW_DASHBOARD_SLINK')
                            ->where([
                                ['channel','NET'],
                                ['flag','P']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();
          $unredeem_count = DB::table('VW_DASHBOARD_SLINK')
                            ->where([
                                ['channel','NET'],
                                ['flag','A']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();


          $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['Settled', 'Not Settled'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#FF6384', '#36A2EB'],
                            'hoverBackgroundColor' => ['#029b17', '#36A2EB'],
                            'data' => [$redeem_count, $unredeem_count]
                        ]
                    ])
                    ->options([]);

         return view('ftrans.ibank.slink',['ibank_slinktrans' => $ibank_trans,
                                            'chartjs' => $chartjs]);
    }
}
