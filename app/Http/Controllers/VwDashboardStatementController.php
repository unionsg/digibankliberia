<?php

namespace App\Http\Controllers;

use App\vw_dashboard_statement;
use Illuminate\Http\Request;

class VwDashboardStatementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\vw_dashboard_statement  $vw_dashboard_statement
     * @return \Illuminate\Http\Response
     */
    public function show(vw_dashboard_statement $vw_dashboard_statement)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\vw_dashboard_statement  $vw_dashboard_statement
     * @return \Illuminate\Http\Response
     */
    public function edit(vw_dashboard_statement $vw_dashboard_statement)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\vw_dashboard_statement  $vw_dashboard_statement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, vw_dashboard_statement $vw_dashboard_statement)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\vw_dashboard_statement  $vw_dashboard_statement
     * @return \Illuminate\Http\Response
     */
    public function destroy(vw_dashboard_statement $vw_dashboard_statement)
    {
        //
    }
}
