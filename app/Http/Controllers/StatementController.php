<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Requester;

use App\Http\Requests;
use Datatables;
use DB;
use Request;
use Session;
use App\User;
use App\vw_dashboard_statement as dashboad_statement;

class StatementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
   /**
     * Display a listing of the mbank statement request.
     *
     * @return \Illuminate\Http\Response
     */
     public function mbank_trans()
    {
        //
        $today = strtoupper(date('Y-m-d'));

        $branches = DB::table('tb_branch')
                        ->where('br_code','>','0')
                        ->get();

         $mbank_stmts = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','MBANK'],
                                 ['POSTING_DATE',$today]
                            ])
                            ->orderBy('POSTING_DATE',$today)
                            ->get();

        $ordinary_count = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','MBANK'],
                                ['STATEMENT_TYPE','ORDINARY'],
                                ['POSTING_DATE',$today]
                            ])
                            ->count();

        $visa_count = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','MBANK'],
                                ['STATEMENT_TYPE','VISA'],
                                ['POSTING_DATE',$today]
                            ])
                            ->count();

        // echo $mbank_stmts; die;

       $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Ordinary Statement', 'Visa Statement'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$ordinary_count,  $visa_count]
            ]
        ])
        ->options([]);



         return view('statement.mbank',['mbank_stmts' => $mbank_stmts,
                                        'branches'=> $branches,
                                        'chartjs'=>$chartjs]);
    }

     public function mbank_search()
    {

        //

        $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $branch = $inputs['branch'];
        $statetype = $inputs['statetype'];

        // echo $startdate; die;



        $branches = DB::table('tb_branch')
                        ->where('br_code','>','0')
                        ->get();

         $mbank_stmts = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','MBANK'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['STATEMENT_TYPE','like','%'.$statetype.'%']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            // ->orderBy('POSTING_DATE','desc')
                            ->get();

        // echo $mbank_stmts; die;

        $ordinary_count = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','MBANK'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['STATEMENT_TYPE','ORDINARY']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

        $visa_count = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','MBANK'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['STATEMENT_TYPE','VISA']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();



       $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Ordinary Statement', 'Visa Statement'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$ordinary_count,  $visa_count]
            ]
        ])
        ->options([]);



         return view('statement.mbank',['mbank_stmts' => $mbank_stmts,
                                        'branches'=> $branches,
                                        'chartjs'=>$chartjs]);
    }

    
    /**
     * Display a listing of the internet banking statement request.
     *
     * @return \Illuminate\Http\Response
     */
     public function ibank_trans()
    {
        //
        $today = strtoupper(date('Y-m-d'));
        
        $branches = DB::table('tb_branch')
                        ->where('br_code','>','0')
                        ->get();

         $ibank_stmts = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','NETBANKING'],
                                ['POSTING_DATE',$today],
                                ['TYPE_OF_ACCT','I']
                            ])
                            ->get();

        $ordinary_count = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','NETBANKING'],
                                ['STATEMENT_TYPE','ORDINARY'],
                                ['POSTING_DATE',$today],
                                ['TYPE_OF_ACCT','I']
                            ])
                            ->count();

        $visa_count = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','NETBANKING'],
                                ['STATEMENT_TYPE','VISA'],
                                ['POSTING_DATE',$today],
                                ['TYPE_OF_ACCT','I']
                            ])
                            ->count();



       $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Ordinary Statement', 'Visa Statement'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$ordinary_count,  $visa_count]
            ]
        ])
        ->options([]);



         return view('statement.ibank',['ibank_stmts' => $ibank_stmts,
                                        'branches'=> $branches,
                                        'chartjs'=>$chartjs]);
    }

     public function ibank_search()
    {

        //

        $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $branch = $inputs['branch'];
        $statetype = $inputs['statetype'];



        $branches = DB::table('tb_branch')
                        ->where('br_code','>','0')
                        ->get();

         $ibank_stmts = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','NETBANKING'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['STATEMENT_TYPE','like','%'.$statetype.'%'],
                                ['TYPE_OF_ACCT','I']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            // ->orderBy('POSTING_DATE','desc')
                            ->get();


        $ordinary_count = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','NETBANKING'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['STATEMENT_TYPE','ORDINARY'],
                                ['TYPE_OF_ACCT','I']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

        $visa_count = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','NETBANKING'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['STATEMENT_TYPE','VISA'],
                                ['TYPE_OF_ACCT','I']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();



       $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Ordinary Statement', 'Visa Statement'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$ordinary_count,  $visa_count]
            ]
        ])
        ->options([]);



         return view('statement.ibank',['ibank_stmts' => $ibank_stmts,
                                        'branches'=> $branches,
                                        'chartjs'=>$chartjs]);
    }


        /**
     * Display a listing of the internet banking statement request.
     *
     * @return \Illuminate\Http\Response
     */
     public function cbank_trans()
    {
        //
        $today = strtoupper(date('Y-m-d'));
        
        $branches = DB::table('tb_branch')
                        ->where('br_code','>','0')
                        ->get();

         $ibank_stmts = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','NETBANKING'],
                                ['POSTING_DATE',$today],
                                ['TYPE_OF_ACCT','C']
                            ])
                            ->get();

        $ordinary_count = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','NETBANKING'],
                                ['STATEMENT_TYPE','ORDINARY'],
                                ['POSTING_DATE',$today],
                                ['TYPE_OF_ACCT','C']
                            ])
                            ->count();

        $visa_count = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','NETBANKING'],
                                ['STATEMENT_TYPE','VISA'],
                                ['POSTING_DATE',$today],
                                ['TYPE_OF_ACCT','C']
                            ])
                            ->count();



       $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Ordinary Statement', 'Visa Statement'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$ordinary_count,  $visa_count]
            ]
        ])
        ->options([]);



         return view('statement.cbank',['ibank_stmts' => $ibank_stmts,
                                        'branches'=> $branches,
                                        'chartjs'=>$chartjs]);
    }

     public function cbank_search()
    {

        //

        $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $branch = $inputs['branch'];
        $statetype = $inputs['statetype'];



        $branches = DB::table('tb_branch')
                        ->where('br_code','>','0')
                        ->get();

         $ibank_stmts = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','NETBANKING'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['STATEMENT_TYPE','like','%'.$statetype.'%'],
                                ['TYPE_OF_ACCT','C']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            // ->orderBy('POSTING_DATE','desc')
                            ->get();


        $ordinary_count = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','NETBANKING'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['STATEMENT_TYPE','ORDINARY'],
                                ['TYPE_OF_ACCT','C']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

        $visa_count = DB::table('vw_dashboard_statement')
                            ->where([
                                ['TERMINAL','NETBANKING'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['STATEMENT_TYPE','VISA'],
                                ['TYPE_OF_ACCT','C']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();



       $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Ordinary Statement', 'Visa Statement'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$ordinary_count,  $visa_count]
            ]
        ])
        ->options([]);



         return view('statement.cbank',['ibank_stmts' => $ibank_stmts,
                                        'branches'=> $branches,
                                        'chartjs'=>$chartjs]);
    }


}
