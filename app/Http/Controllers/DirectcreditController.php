<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Requester;

use Datatables;
use DB;
use Request;
use Session;

class DirectcreditController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
   /**
     * Display a listing of the mbank ach trans.
     *
     * @return \Illuminate\Http\Response
     */
     public function mbank_trans()
    {
        //
        $today = strtoupper(date('Y-m-d'));
        $banks = DB::table('code_desc')
                    ->where('code_type','BNC')
                    ->whereNotIn('actual_code', ['003', '000'])
                    ->get();

        // echo $banks;
         $mbank_trans = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','BLINK'],
                                ['posting_sys_date','>=',$today]
                            ])
                            ->orderBy('posting_sys_date','desc')
                            ->get();

        $settled_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','BLINK'],
                                ['flag','A'],
                                ['posting_sys_date','>=',$today]
                            ])
                            ->count();

        $psettled_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','BLINK'],
                                ['flag','P'],
                                ['posting_sys_date','>=',$today]
                            ])
                            ->count();

        $rev_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','BLINK'],
                                ['flag','V'],
                                ['posting_sys_date','>=',$today]
                            ])
                            ->count();

         $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['SETTLED', 'PENDING SETTLEMENT','REVERSED'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#029b17', '#008a9b','#db2c2c'],
                            'hoverBackgroundColor' => ['#029b17', '#008a9b','#db2c2c'],
                            'data' => [$settled_count, $psettled_count,$rev_count]
                        ]
                    ])
                    ->options([]);

        
         return view('Dcredit.mbank',['mbank_trans' => $mbank_trans,
                                      'chartjs' => $chartjs,
                                      'banks'=>$banks]);
    }
    /**
     * Display a listing of the mbank ach trans.
     *
     * @return \Illuminate\Http\Response
     */
     public function mbank_search()
    {
        //

        $inputs = Request::all();


        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $status = $inputs['status'];
        $bank = $inputs['bank'];

       

         $mbank_trans = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','BLINK'],
                                ['flag','like','%'.$status.'%'],
                                ['CREDIT_BANK','like','%'.$bank.'%']
                                ])
                            ->whereBetween('posting_sys_date',[$startdate,$enddate])
                            ->orderBy('posting_sys_date','desc')
                            ->get();

        $settled_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','BLINK'],
                                ['flag','A'],
                                ['CREDIT_BANK','like','%'.$bank.'%']
                                ])
                            ->whereBetween('posting_sys_date',[$startdate,$enddate])                         
                            ->count();

        $psettled_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','BLINK'],
                                ['flag','P'],
                                ['CREDIT_BANK','like','%'.$bank.'%']
                                ])
                            ->whereBetween('posting_sys_date',[$startdate,$enddate])
                            ->count();

        $rev_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','BLINK'],
                                ['flag','V'],
                                ['CREDIT_BANK','like','%'.$bank.'%']
                                ])
                            ->whereBetween('posting_sys_date',[$startdate,$enddate])
                            ->count();

        $banks = DB::table('code_desc')
                    ->where('code_type','BNC')
                    ->whereNotIn('actual_code', ['003', '000'])
                    ->get();

         $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['SETTLED', 'PENDING SETTLEMENT','REVERSED'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#029b17', '#008a9b','#db2c2c'],
                            'hoverBackgroundColor' => ['#029b17', '#008a9b','#db2c2c'],
                            'data' => [$settled_count, $psettled_count,$rev_count]
                        ]
                    ])
                    ->options([]);

        
         return view('Dcredit.mbank',['mbank_trans' => $mbank_trans,
                                      'chartjs' => $chartjs,
                                      'banks'=>$banks]);
    }
    /**
     * Display a listing of the ibank ach trans.
     *
     * @return \Illuminate\Http\Response
     */
     public function ibank_trans()
    {
        //
        $today = strtoupper(date('Y-m-d'));

        $banks = DB::table('code_desc')
                    ->where('code_type','BNC')
                    ->whereNotIn('actual_code', ['003', '000'])
                    ->get();

        // echo $banks;
         $ibank_trans = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                    ['posted_by','NET'],
                                    ['posting_sys_date','>=',$today],
                                    ['type_of_acct','I']
                                ])
                            ->orderBy('posting_sys_date','desc')
                            ->get();

        // echo $ibank_trans; die;

        $settled_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','NET'],
                                ['flag','A'],
                                ['posting_sys_date','>=',$today],
                                ['type_of_acct','I']
                            ])
                            ->count();

        $psettled_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','NET'],
                                ['flag','P'],
                                ['posting_sys_date','>=',$today],
                                ['type_of_acct','I']
                            ])
                            ->count();

        $rev_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','NET'],
                                ['flag','V'],
                                ['posting_sys_date','>=',$today],
                                ['type_of_acct','I']
                            ])
                            ->count();

         $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['SETTLED', 'PENDING SETTLEMENT','REVERSED'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#029b17', '#008a9b','#db2c2c'],
                            'hoverBackgroundColor' => ['#029b17', '#008a9b','#db2c2c'],
                            'data' => [$settled_count, $psettled_count,$rev_count]
                        ]
                    ])
                    ->options([]);

        
         return view('Dcredit.ibank',['ibank_trans' => $ibank_trans,
                                      'chartjs' => $chartjs,
                                      'banks'=>$banks]);
    }
    /**
     * Display a listing of the ibank ach trans.
     *
     * @return \Illuminate\Http\Response
     */
     public function ibank_search()
    {
        //

        $inputs = Request::all();


        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $status = $inputs['status'];
        $bank = $inputs['bank'];

       

         $ibank_trans = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','NET'],
                                ['flag','like','%'.$status.'%'],
                                ['CREDIT_BANK','like','%'.$bank.'%'],
                                ['type_of_acct','I']
                                ])
                            ->whereBetween('posting_sys_date',[$startdate,$enddate])
                            ->orderBy('posting_sys_date','desc')
                            ->get();

        $settled_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','NET'],
                                ['flag','A'],
                                ['CREDIT_BANK','like','%'.$bank.'%'],
                                ['type_of_acct','I']
                                ])
                            ->whereBetween('posting_sys_date',[$startdate,$enddate])                         
                            ->count();

        $psettled_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','NET'],
                                ['flag','P'],
                                ['CREDIT_BANK','like','%'.$bank.'%'],
                                ['type_of_acct','I']
                                ])
                            ->whereBetween('posting_sys_date',[$startdate,$enddate])
                            ->count();

        $rev_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','NET'],
                                ['flag','V'],
                                ['CREDIT_BANK','like','%'.$bank.'%'],
                                ['type_of_acct','I']
                                ])
                            ->whereBetween('posting_sys_date',[$startdate,$enddate])
                            ->count();

        $banks = DB::table('code_desc')
                    ->where('code_type','BNC')
                    ->whereNotIn('actual_code', ['003', '000'])
                    ->get();

         $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['SETTLED', 'PENDING SETTLEMENT','REVERSED'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#029b17', '#008a9b','#db2c2c'],
                            'hoverBackgroundColor' => ['#029b17', '#008a9b','#db2c2c'],
                            'data' => [$settled_count, $psettled_count,$rev_count]
                        ]
                    ])
                    ->options([]);

        
         return view('Dcredit.ibank',['ibank_trans' => $ibank_trans,
                                      'chartjs' => $chartjs,
                                      'banks'=>$banks]);
    }

     /**
     * Display a listing of the cbank ach trans.
     *
     * @return \Illuminate\Http\Response
     */
     public function cbank_trans()
    {
        //
        $today = strtoupper(date('Y-m-d'));

        $banks = DB::table('code_desc')
                    ->where('code_type','BNC')
                    ->whereNotIn('actual_code', ['003', '000'])
                    ->get();

        // echo $banks;
         $ibank_trans = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                    ['posted_by','NET'],
                                    ['posting_sys_date','>=',$today],
                                    ['type_of_acct','C']
                                ])
                            ->orderBy('posting_sys_date','desc')
                            ->get();

        // echo $ibank_trans; die;

        $settled_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','NET'],
                                ['flag','A'],
                                ['posting_sys_date','>=',$today],
                                ['type_of_acct','C']
                            ])
                            ->count();

        $psettled_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','NET'],
                                ['flag','P'],
                                ['posting_sys_date','>=',$today],
                                ['type_of_acct','C']
                            ])
                            ->count();

        $rev_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','NET'],
                                ['flag','V'],
                                ['posting_sys_date','>=',$today],
                                ['type_of_acct','C']
                            ])
                            ->count();

         $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['SETTLED', 'PENDING SETTLEMENT','REVERSED'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#029b17', '#008a9b','#db2c2c'],
                            'hoverBackgroundColor' => ['#029b17', '#008a9b','#db2c2c'],
                            'data' => [$settled_count, $psettled_count,$rev_count]
                        ]
                    ])
                    ->options([]);

        
         return view('Dcredit.cbank',['ibank_trans' => $ibank_trans,
                                      'chartjs' => $chartjs,
                                      'banks'=>$banks]);
    }
    /**
     * Display a listing of the cbank ach trans.
     *
     * @return \Illuminate\Http\Response
     */
     public function cbank_search()
    {
        //

        $inputs = Request::all();


        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $status = $inputs['status'];
        $bank = $inputs['bank'];

       

         $ibank_trans = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','NET'],
                                ['flag','like','%'.$status.'%'],
                                ['CREDIT_BANK','like','%'.$bank.'%'],
                                ['type_of_acct','C']
                                ])
                            ->whereBetween('posting_sys_date',[$startdate,$enddate])
                            ->orderBy('posting_sys_date','desc')
                            ->get();

        $settled_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','NET'],
                                ['flag','A'],
                                ['CREDIT_BANK','like','%'.$bank.'%'],
                                ['type_of_acct','C']
                                ])
                            ->whereBetween('posting_sys_date',[$startdate,$enddate])                         
                            ->count();

        $psettled_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','NET'],
                                ['flag','P'],
                                ['CREDIT_BANK','like','%'.$bank.'%'],
                                ['type_of_acct','C']
                                ])
                            ->whereBetween('posting_sys_date',[$startdate,$enddate])
                            ->count();

        $rev_count = DB::table('VW_DASHBOARD_ACH')
                            ->where([
                                ['posted_by','NET'],
                                ['flag','V'],
                                ['CREDIT_BANK','like','%'.$bank.'%'],
                                ['type_of_acct','C']
                                ])
                            ->whereBetween('posting_sys_date',[$startdate,$enddate])
                            ->count();

        $banks = DB::table('code_desc')
                    ->where('code_type','BNC')
                    ->whereNotIn('actual_code', ['003', '000'])
                    ->get();

         $chartjs = app()->chartjs
                    ->name('pieChartTest')
                    ->type('pie')
                    ->size(['width' => 200, 'height' => 100])
                    ->labels(['SETTLED', 'PENDING SETTLEMENT','REVERSED'])
                    ->datasets([
                        [
                            'backgroundColor' => ['#029b17', '#008a9b','#db2c2c'],
                            'hoverBackgroundColor' => ['#029b17', '#008a9b','#db2c2c'],
                            'data' => [$settled_count, $psettled_count,$rev_count]
                        ]
                    ])
                    ->options([]);

        
         return view('Dcredit.cbank',['ibank_trans' => $ibank_trans,
                                      'chartjs' => $chartjs,
                                      'banks'=>$banks]);
    }

}
