<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Requester;

use App\Http\Requests;
use Datatables;
use DB;
use Request;
use App\User;
use App\user_profile as userprofile;

class UserprofileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get registered users from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function registered_users()
    {
        //
        $today = strtoupper(date('Y-m-d'));
         $reg_users = DB::table('VW_DASHBOARD_DIGITALUSERS')
                           ->where('approval_date', $today)
         					->orderBy('approval_date','desc')
                            ->get();

         return view('users.reg-users',['regusers' => $reg_users]);
    }

     public function registered_srch()
    {

         $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        
         $reg_users = DB::table('VW_DASHBOARD_DIGITALUSERS')
                            ->whereBetween('approval_date', [$startdate, $enddate])
                            ->orderBy('approval_date','desc')
                            ->get();
         return view('users.reg-users',['regusers' => $reg_users]);
    }

    public function locked_users()
    {
        //
         $lock_users = DB::table('VW_DASHBOARD_DIGITALUSERS')
         					->where('login_try','>=','3')
                            ->get();
        
         return view('users.lock-users',['lkusers' => $lock_users]);
    }

     public function blocked_users()
    {
        //
        $blk_cnt = DB::table('channel_parameters')
                    ->value('userid_try');

         $block_users = DB::table('VW_DASHBOARD_DIGITALUSERS')
                            ->where('unlock_count','>=',$blk_cnt)
                            ->get();
         return view('users.block-users',['blkusers' => $block_users]);
    }

      public function active_users()
    {
        //
        $inactive_v = DB::table('channel_parameters')
                        ->value('INACTIVE_DAYS');       

        $inactive_users = DB::table('VW_DASHBOARD_DIGITALUSERS')
                            ->where('INACTIVE_DAYS','>=',$inactive_v)
                            ->get();

        $active_users = DB::table('VW_DASHBOARD_DIGITALUSERS')
                            ->where('INACTIVE_DAYS','<',$inactive_v)
                            ->get();

         return view('users.active-users',['activeusers' => $active_users,
                                          'inactiveusers' => $inactive_users]);
    }
}
