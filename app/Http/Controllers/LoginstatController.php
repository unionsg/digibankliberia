<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Requester;

use Datatables;
use DB;
use Request;
use Session;

class LoginstatController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mbankindex()
    {
        //
        $today = strtoupper(date('Y-m-d'));
        $yesterday = strtoupper(date('d-M-Y',strtotime("-1 days")));

     

        $mbanksum = DB::table('vw_dashboard_loginstat')
                            ->select('userid',DB::raw('SUM(SUCCESS_COUNT) as SUCCESS,SUM(UNSUCCESS_COUNT) as FAIL,sum(success_count)+sum(unsuccess_count) as total'))
                            ->where([
                                ['channel','MBANK'],
                                ['POSTING_DATE',$today]
                            ])                           
                            ->groupBy('userid')
                            ->get();

    $mbanksuccess = DB::table('vw_dashboard_loginstat')
                        ->select(DB::raw('SUM(SUCCESS_COUNT) as success'))
                        ->where([
                            ['channel','MBANK'],
                            ['POSTING_DATE',$today]
                        ])
                        ->value('success');

     $mbankfail = DB::table('vw_dashboard_loginstat')
                        ->select(DB::raw('SUM(UNSUCCESS_COUNT) as fail'))
                        ->where([
                            ['channel','MBANK'],
                            ['POSTING_DATE',$today]
                        ])
                        ->value('fail');
        
        $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Success', 'Failed'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$mbanksuccess,  $mbankfail]
            ]
        ])
        ->options([]);
        // echo $mbanksum; die;
        return view('loginstat.summary.mbank',['mbank_stats' => $mbanksum,
                                            'chartjs'=>$chartjs]);
    }


     public function mbanksearch()
    {
        //
         $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];

        $date = 'Date between '.$startdate.' - '.$enddate;

        

        $mbanksum = DB::table('vw_dashboard_loginstat')
                            ->select('userid',DB::raw('SUM(SUCCESS_COUNT) as SUCCESS,SUM(UNSUCCESS_COUNT) as FAIL,sum(success_count)+sum(unsuccess_count) as total'))
                            ->where('channel','MBANK')
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->groupBy('userid')
                            ->get();
        // echo $mbanksum; die;
       
    $mbanksuccess = DB::table('vw_dashboard_loginstat')
                        ->select(DB::raw('SUM(SUCCESS_COUNT) as success'))
                        ->where([
                            ['channel','MBANK']
                        ])
                        ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                        ->value('success');

     $mbankfail = DB::table('vw_dashboard_loginstat')
                        ->select(DB::raw('SUM(UNSUCCESS_COUNT) as fail'))
                        ->where([
                            ['channel','MBANK']
                        ])
                        ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                        ->value('fail');
        
        $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Success', 'Failed'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$mbanksuccess,  $mbankfail]
            ]
        ])
        ->options([]);
        // echo $mbanksum; die;
        return view('loginstat.summary.mbank',['mbank_stats' => $mbanksum,
                                            'chartjs'=>$chartjs]);

    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mbankdetailindex()
    {
        //
        $today = strtoupper(date('Y-m-d'));

     

        $mbanklogs = DB::table('VW_DASHBOARD_LOGIN_LOGS')                           
                            ->where([
                                ['channel','MBANK'],
                                ['POSTING_DATE',$today]
                            ])                      
                            ->get();
        // echo $mbanksum; die;
        return view('loginstat.details.mbank',['mbank_logs' => $mbanklogs]);
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mbankdetailsearch()
    {
        //
        $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];

        $date = 'Date between '.$startdate.' - '.$enddate;

     

        $mbanklogs = DB::table('VW_DASHBOARD_LOGIN_LOGS')                           
                            ->where('channel','MBANK')
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])                           
                            ->get();
        // echo $mbanksum; die;
        return view('loginstat.details.mbank',['mbank_logs' => $mbanklogs]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ibankindex()
    {
        //
        $today = strtoupper(date('Y-m-d'));
     

        $ibanksum = DB::table('vw_dashboard_loginstat')
                            ->select('userid',DB::raw('SUM(SUCCESS_COUNT) as SUCCESS,SUM(UNSUCCESS_COUNT) as FAIL,sum(success_count)+sum(unsuccess_count) as total'))
                            ->where([
                                ['channel','NETBANK'],
                                ['POSTING_DATE',$today],
                                ['TYPE_OF_ACCT','I']
                            ])
                            ->groupBy('userid')
                            ->get();

      $ibanksuccess = DB::table('vw_dashboard_loginstat')
                        ->select(DB::raw('SUM(SUCCESS_COUNT) as success'))
                        ->where([
                            ['channel','NETBANK'],
                            ['POSTING_DATE',$today],
                            ['TYPE_OF_ACCT','I']
                        ])
                        ->value('success');

     $ibankfail = DB::table('vw_dashboard_loginstat')
                        ->select(DB::raw('SUM(UNSUCCESS_COUNT) as fail'))
                        ->where([
                            ['channel','NETBANK'],
                            ['POSTING_DATE',$today],
                            ['TYPE_OF_ACCT','I']
                        ])
                        ->value('fail');
        
        $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Success', 'Failed'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$ibanksuccess,  $ibankfail]
            ]
        ])
        ->options([]);

        return view('loginstat.summary.ibank',['ibank_stats' => $ibanksum,
                                                'chartjs'=>$chartjs]);
    }


     public function ibanksearch()
    {
        //
         $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];

        $date = 'Date between '.$startdate.' - '.$enddate;

      $ibanksuccess = DB::table('vw_dashboard_loginstat')
                    ->select(DB::raw('SUM(SUCCESS_COUNT) as success'))
                    ->where([
                        ['channel','NETBANK'],
                        ['TYPE_OF_ACCT','I']
                    ])
                    ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                    ->value('success');

     $ibankfail = DB::table('vw_dashboard_loginstat')
                        ->select(DB::raw('SUM(UNSUCCESS_COUNT) as fail'))
                        ->where([
                            ['channel','NETBANK'],
                            ['TYPE_OF_ACCT','I']
                        ])
                        ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                        ->value('fail');

         $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Success', 'Failed'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$ibanksuccess,  $ibankfail]
            ]
        ])
        ->options([]);

        $ibanksum = DB::table('vw_dashboard_loginstat')
                            ->select('userid',DB::raw('SUM(SUCCESS_COUNT) as SUCCESS,SUM(UNSUCCESS_COUNT) as FAIL,sum(success_count)+sum(unsuccess_count) as total'))
                            ->where([
                                ['channel','NETBANK'],
                                ['TYPE_OF_ACCT','I']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->groupBy('userid')
                            ->get();
        // echo $ibanksum; die;
        return view('loginstat.summary.ibank',['ibank_stats' => $ibanksum,
                                                'chartjs'=>$chartjs]);

    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ibankdetailindex()
    {
        //
        $today = strtoupper(date('Y-m-d'));
       // $yesterday = strtoupper(date('d-M-Y',strtotime("-1 days")));

     

        $ibanklogs = DB::table('VW_DASHBOARD_LOGIN_LOGS')                           
                            ->where([
                                ['channel','NETBANK'],
                                ['POSTING_DATE',$today],
                                ['TYPE_OF_ACCT','I']
                            ])                       
                            ->get();
        // echo $ibanksum; die;
        return view('loginstat.details.ibank',['ibank_logs' => $ibanklogs]);
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ibankdetailsearch()
    {
        //
        $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];

        $date = 'Date between '.$startdate.' - '.$enddate;

     

        $ibanklogs = DB::table('VW_DASHBOARD_LOGIN_LOGS')                           
                            ->where([
                                ['channel','NETBANK'],
                                ['TYPE_OF_ACCT','I']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])                           
                            ->get();
        // echo $ibanksum; die;
        return view('loginstat.details.ibank',['ibank_logs' => $ibanklogs]);
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cbankindex()
    {
        //
        $today = strtoupper(date('Y-m-d'));
     

        $ibanksum = DB::table('vw_dashboard_loginstat')
                            ->select('userid',DB::raw('SUM(SUCCESS_COUNT) as SUCCESS,SUM(UNSUCCESS_COUNT) as FAIL,sum(success_count)+sum(unsuccess_count) as total'))
                            ->where([
                                ['channel','NETBANK'],
                                ['POSTING_DATE',$today],
                                ['TYPE_OF_ACCT','C']
                            ])
                            ->groupBy('userid')
                            ->get();

      $ibanksuccess = DB::table('vw_dashboard_loginstat')
                        ->select(DB::raw('SUM(SUCCESS_COUNT) as success'))
                        ->where([
                            ['channel','NETBANK'],
                            ['POSTING_DATE',$today],
                            ['TYPE_OF_ACCT','C']
                        ])
                        ->value('success');

     $ibankfail = DB::table('vw_dashboard_loginstat')
                        ->select(DB::raw('SUM(UNSUCCESS_COUNT) as fail'))
                        ->where([
                            ['channel','NETBANK'],
                            ['POSTING_DATE',$today],
                            ['TYPE_OF_ACCT','C']
                        ])
                        ->value('fail');
        
        $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Success', 'Failed'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$ibanksuccess,  $ibankfail]
            ]
        ])
        ->options([]);

        return view('loginstat.summary.cbank',['ibank_stats' => $ibanksum,
                                                'chartjs'=>$chartjs]);
    }


     public function cbanksearch()
    {
        //
         $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];

        $date = 'Date between '.$startdate.' - '.$enddate;

      $ibanksuccess = DB::table('vw_dashboard_loginstat')
                    ->select(DB::raw('SUM(SUCCESS_COUNT) as success'))
                    ->where([
                        ['channel','NETBANK'],
                        ['TYPE_OF_ACCT','C']
                    ])
                    ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                    ->value('success');

     $ibankfail = DB::table('vw_dashboard_loginstat')
                        ->select(DB::raw('SUM(UNSUCCESS_COUNT) as fail'))
                        ->where([
                            ['channel','NETBANK'],
                            ['TYPE_OF_ACCT','C']
                        ])
                        ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                        ->value('fail');

         $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Success', 'Failed'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$ibanksuccess,  $ibankfail]
            ]
        ])
        ->options([]);

        $ibanksum = DB::table('vw_dashboard_loginstat')
                            ->select('userid',DB::raw('SUM(SUCCESS_COUNT) as SUCCESS,SUM(UNSUCCESS_COUNT) as FAIL,sum(success_count)+sum(unsuccess_count) as total'))
                            ->where([
                                ['channel','NETBANK'],
                                ['TYPE_OF_ACCT','C']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->groupBy('userid')
                            ->get();
        // echo $ibanksum; die;
        return view('loginstat.summary.cbank',['ibank_stats' => $ibanksum,
                                                'chartjs'=>$chartjs]);

    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cbankdetailindex()
    {
        //
        $today = strtoupper(date('Y-m-d'));
       // $yesterday = strtoupper(date('d-M-Y',strtotime("-1 days")));

     

        $ibanklogs = DB::table('VW_DASHBOARD_LOGIN_LOGS')                           
                            ->where([
                                ['channel','NETBANK'],
                                ['POSTING_DATE',$today],
                                ['TYPE_OF_ACCT','C']
                            ])                       
                            ->get();
        // echo $ibanksum; die;
        return view('loginstat.details.cbank',['ibank_logs' => $ibanklogs]);
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cbankdetailsearch()
    {
        //
        $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];

        $date = 'Date between '.$startdate.' - '.$enddate;

     

        $ibanklogs = DB::table('VW_DASHBOARD_LOGIN_LOGS')                           
                            ->where([
                                ['channel','NETBANK'],
                                ['TYPE_OF_ACCT','C']
                            ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])                           
                            ->get();
        // echo $ibanksum; die;
        return view('loginstat.details.cbank',['ibank_logs' => $ibanklogs]);
    }
}
