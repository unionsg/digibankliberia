<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Requester;

use Datatables;
use DB;
use Request;
use Session;

class AuditController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mbankindex()
    {
        //
        $today = strtoupper(date('Y-m-d'));

        $audit_logs = DB::table('mbank_user_daily_audit')
                        ->where('POSTING_SYS_TIME','>=',$today)
                        ->get();

        // echo $audit_logs; die;
         return view('audit.mbank',['audit_logs' => $audit_logs]);


    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mbanksearch()
    {
        //
        $inputs = Request::all();


        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];

        $audit_logs = DB::table('mbank_user_daily_audit')
                        ->whereBetween('POSTING_SYS_TIME',[$startdate,$enddate])
                        ->get();

        // echo $audit_logs; die;
         return view('audit.mbank',['audit_logs' => $audit_logs]);


    }


     public function ibankindex()
    {
        //
        $today = strtoupper(date('Y-m-d'));

        $audit_logs = DB::table('internet_audit_table')
                        ->where('DATE_LOGGED','>=',$today)
                        ->get();

        // echo $audit_logs; die;
         return view('audit.ibank',['audit_logs' => $audit_logs]);


    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ibanksearch()
    {
        //
        $inputs = Request::all();


        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];

        $audit_logs = DB::table('internet_audit_table')
                        ->whereBetween('DATE_LOGGED',[$startdate,$enddate])
                        ->get();

        // echo $audit_logs; die;
         return view('audit.ibank',['audit_logs' => $audit_logs]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
