<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Requester;

use App\Http\Requests;
use Datatables;
use DB;
use PDF;
use Request;
use App\User;
use App\vw_dashboard_logincount as loginCount;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $today = strtoupper(date('Y-m-d'));
        $yesterday = strtoupper(date('Y-m-d',strtotime("-1 days")));

        // $inactive_v = DB::table('channel_parameters')
        //                 ->value('INACTIVE_DAYS');

       
     

        $mbank_loginsum = DB::table('vw_dashboard_loginsum')
                                            ->where('channel','MBANK')  
                                            ->get();
        $netbank_loginsum = DB::table('vw_dashboard_loginsum')
                                            ->where('channel','NETBANKING')  
                                            ->get();
        $signup_sum = DB::table('vw_dashboard_signupsum') 
                                            ->orderBy('SIGNUP_DATE','asc')
                                            ->get();

        // $locks = DB::table('VW_DASHBOARD_LOCK') -> get();

        //internet banking total revenue
        $netbank_revenue = DB::table('vw_dashboard_revenue')
                            ->select(DB::raw('SUM(REVENUE) AS REVENUE'))
                            ->where([
                                ['channel','NETBANK'],
                                ['posting_date',$today]
                             ])
                            ->value('revenue');
     
        //mobile banking total revenue
       $mbank_revenue = DB::table('vw_dashboard_revenue')
                            ->select(DB::raw('SUM(REVENUE) AS REVENUE'))
                            ->where([
                                ['channel','MBANK'],
                                ['posting_date',$today]
                            ])
                            ->value('revenue');

         //mteller total revenue
       $mteller_revenue = DB::table('vw_dashboard_revenue')
                            ->select(DB::raw('SUM(REVENUE) AS REVENUE'))
                            ->where([
                                ['channel','MTELLER'],
                                ['posting_date',$today]
                            ])
                            ->value('revenue');

       
       $netbank_totals = DB::table('vw_dashboard_today')
                        ->where('channel','NET')
                        ->get();

       $mbank_totals = DB::table('vw_dashboard_today')
                        ->where('channel','MOB')
                        ->get();

       // $active_users = DB::table('VW_DASHBOARD_DIGIUSERS_STATUS')
       //                  ->get();

        // echo $active_users; die;

       // $inactive_users = DB::table('VW_DASHBOARD_DIGITALUSERS')
       //                  ->select(DB::raw('count(user_id) as i_num'))
       //                  ->where('INACTIVE_DAYS','>=',$inactive_v)
       //                  ->get();


        

      $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('doughnut')
        ->size(['width' => 400, 'height' => 200])
        ->labels(['MOBILE BANKING', 'INTERNET BANKING','MI YONE TELLER'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB','#36EDEB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB','#36EDEB'],
                'data' => [$mbank_revenue, $netbank_revenue,$mteller_revenue]
            ]
        ])
        ->options([]);

        $revenue = DB::table('vw_dashboard_revenue')
                            ->select('channel',DB::raw('SUM(REVENUE) AS REVENUE'))
                            ->where('POSTING_DATE',$today)
                            ->groupBy('channel')
                            ->get();

        // echo $netbank_totals; die;

        $active_users ='';

        return view('home',['mbank_todays' => $mbank_loginsum,
                            'netbank_todays'=>$netbank_loginsum,
                            'signup_sums'=>$signup_sum,
                            'chartjs'=>$chartjs,
                            'revenues'=>$revenue,
                            'netbank_totals'=>$netbank_totals,
                            'mbank_totals'=>$mbank_totals,
                            'locks'=>$locks,
                            'active_users'=>$active_users,
                            // 'inactive_users'=>$inactive_users
                            ]);
    }

     public function export_pdf()
    {
        $today = strtoupper(date('Y-m-d'));
        $yesterday = strtoupper(date('Y-m-d',strtotime("-1 days")));
        $inactive_v = DB::table('channel_parameters')
                        ->value('INACTIVE_DAYS');

       
     

        $mbank_loginsum = DB::table('vw_dashboard_loginsum')
                                            ->where('channel','MBANK')  
                                            ->get();
        $netbank_loginsum = DB::table('vw_dashboard_loginsum')
                                            ->where('channel','NETBANKING')  
                                            ->get();
        $signup_sum = DB::table('vw_dashboard_signupsum') 
                                            ->orderBy('SIGNUP_DATE','asc')
                                            ->get();

        $locks = DB::table('VW_DASHBOARD_LOCK') -> get();

        $netbank_revenue = DB::table('vw_dashboard_revenue')
                            ->select(DB::raw('SUM(REVENUE) AS REVENUE'))
                            ->where([
                                ['channel','NETBANK'],
                                ['posting_date',$today]
                             ])
                            ->value('revenue');
        // echo $netbank_revenue; die;

       $mbank_revenue = DB::table('vw_dashboard_revenue')
                            ->select(DB::raw('SUM(REVENUE) AS REVENUE'))
                            ->where([
                                ['channel','MBANK'],
                                ['posting_date',$today]
                            ])
                            ->value('revenue');

       
       $netbank_totals = DB::table('vw_dashboard_today')
                        ->where('channel','NET')
                        ->get();

       $mbank_totals = DB::table('vw_dashboard_today')
                        ->where('channel','MOB')
                        ->get();

       $active_users = DB::table('VW_DASHBOARD_DIGIUSERS_STATUS')
                        ->get();

        // echo $active_users; die;

       // $inactive_users = DB::table('VW_DASHBOARD_DIGITALUSERS')
       //                  ->select(DB::raw('count(user_id) as i_num'))
       //                  ->where('INACTIVE_DAYS','>=',$inactive_v)
       //                  ->get();


        

      $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('doughnut')
        ->size(['width' => 400, 'height' => 200])
        ->labels(['MOBILE BANKING', 'INTERNET BANKING'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$mbank_revenue, $netbank_revenue]
            ]
        ])
        ->options([]);

        $revenue = DB::table('vw_dashboard_revenue')
                            ->select('channel',DB::raw('SUM(REVENUE) AS REVENUE'))
                            ->where('POSTING_DATE',$today)
                            ->groupBy('channel')
                            ->get();

        // echo $netbank_totals; die;

         $pdf = PDF::loadView('pdf.invoice', ['mbank_todays' => $mbank_loginsum,
                            'netbank_todays'=>$netbank_loginsum,
                            'signup_sums'=>$signup_sum,
                            'chartjs'=>$chartjs,
                            'revenues'=>$revenue,
                            'netbank_totals'=>$netbank_totals,
                            'mbank_totals'=>$mbank_totals,
                            'locks'=>$locks,
                            'active_users'=>$active_users,
                            // 'inactive_users'=>$inactive_users
                            ]);

         return $pdf->stream('customers.pdf');
       
    }
}
