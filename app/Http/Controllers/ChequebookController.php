<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Requester;

use App\Http\Requests;
use Datatables;
use DB;
use Request;
use Session;
use App\User;

class ChequebookController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function branches()
    {

        $branches = DB::table('tb_branch')
                        ->where('br_code','>','0')
                        ->get();
        return $branches;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     /**
     * Display a listing of the mbank chequebook request.
     *
     * @return \Illuminate\Http\Response
     */
     public function mbank_trans()
    {
        //
         $today = strtoupper(date('Y-m-d'));

         $branches = DB::table('tb_branch')
                        ->where('br_code','>','0')
                        ->get();

         $mbank_chqbks = DB::table('VW_DASHBOARD_CHEQUEBOOK')
                            ->where([
                                ['CHANNEL','MBANK'],
                                ['POSTING_DATE',$today],
                            ])
                            ->orderBy('POSTING_DATE','desc')
                            ->get();

         $issued_count = DB::table('VW_DASHBOARD_CHEQUEBOOK')
                            ->where([
                                ['CHANNEL','MBANK'],
                                ['POSTING_DATE',$today],
                                ['FLAG','I']
                            ])
                            ->count();

         $unissued_count =  DB::table('VW_DASHBOARD_CHEQUEBOOK')
                            ->where([
                                ['CHANNEL','MBANK'],
                                ['POSTING_DATE',$today],
                                ['FLAG','<>','I']
                            ])
                            ->count();

           
          $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['ISSUED', 'NOT ISSUED'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$issued_count,$unissued_count]
            ]
        ])
        ->options([]);
                        

         return view('chequebk.mbank',['mbank_chqbks' => $mbank_chqbks,
                                        'branches'=>$branches,
                                        'chartjs'=>$chartjs]);
    }
    
    /**
     * Search listing of the mbank chequebook request per search parameters.
     *
     * @return \Illuminate\Http\Response
     */
     public function mbank_search()
    {
       
        //
         $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $branch = $inputs['branch'];

         $branches = DB::table('tb_branch')
                        ->where('br_code','>','0')
                        ->get();

         $mbank_chqbks = DB::table('VW_DASHBOARD_CHEQUEBOOK')
                            ->where([
                                ['CHANNEL','MBANK'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%']
                                 ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->get();

         $issued_count = DB::table('VW_DASHBOARD_CHEQUEBOOK')
                             ->where([
                                ['CHANNEL','MBANK'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['FLAG','I']
                                 ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

         $unissued_count =  DB::table('VW_DASHBOARD_CHEQUEBOOK')
                             ->where([
                                ['CHANNEL','MBANK'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['FLAG','<>','I']
                                 ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

           
          $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['ISSUED', 'NOT ISSUED'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$issued_count,$unissued_count]
            ]
        ])
        ->options([]);
                        

         return view('chequebk.mbank',['mbank_chqbks' => $mbank_chqbks,
                                        'branches'=>$branches,
                                        'chartjs'=>$chartjs]);
    }
     /**
     * Display a listing of the ibank chequebook request.
     *
     * @return \Illuminate\Http\Response
     */
     public function ibank_trans()
    {
        //
        $today = strtoupper(date('Y-m-d'));

         $branches = DB::table('tb_branch')
                        ->where('br_code','>','0')
                        ->get();

         $ibank_chqbks = DB::table('VW_DASHBOARD_CHEQUEBOOK')
                            ->where([
                                ['CHANNEL','INTBGATEWAY'],
                                ['POSTING_DATE','>=',$today],
                                ['TYPE_OF_ACCT','I']
                            ])
                            ->orderBy('POSTING_DATE','desc')
                            ->get();

         $issued_count = DB::table('VW_DASHBOARD_CHEQUEBOOK')
                             ->where([
                                ['CHANNEL','INTBGATEWAY'],
                                ['POSTING_DATE','>=',$today],
                                ['TYPE_OF_ACCT','I'],
                                ['FLAG','I']
                            ])
                            ->count();

         $unissued_count =  DB::table('VW_DASHBOARD_CHEQUEBOOK')
                             ->where([
                                ['CHANNEL','INTBGATEWAY'],
                                ['POSTING_DATE','>=',$today],
                                ['TYPE_OF_ACCT','I'],
                                ['FLAG','<>','I']
                            ])
                            ->count();

           
          $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['ISSUED', 'NOT ISSUED'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$issued_count,$unissued_count]
            ]
        ])
        ->options([]);
                        

         return view('chequebk.ibank',['ibank_chqbks' => $ibank_chqbks,
                                        'branches'=>$branches,
                                        'chartjs'=>$chartjs]);
    }
    /**
     * Search listing of the ibank chequebook request per search parameters.
     *
     * @return \Illuminate\Http\Response
     */
     public function ibank_search()
    {
       
        //
         $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $branch = $inputs['branch'];

         $branches = DB::table('tb_branch')
                        ->where('br_code','>','0')
                        ->get();

         $ibank_chqbks = DB::table('VW_DASHBOARD_CHEQUEBOOK')
                            ->where([
                                ['CHANNEL','INTBGATEWAY'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['TYPE_OF_ACCT','I']
                                 ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->get();

         $issued_count = DB::table('VW_DASHBOARD_CHEQUEBOOK')
                             ->where([
                                ['CHANNEL','INTBGATEWAY'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['TYPE_OF_ACCT','I'],
                                ['FLAG','I']
                                 ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

         $unissued_count =  DB::table('VW_DASHBOARD_CHEQUEBOOK')
                             ->where([
                                ['CHANNEL','INTBGATEWAY'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['TYPE_OF_ACCT','I'],
                                ['FLAG','<>','I']
                                 ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

           
          $chartjs = app()->chartjs
            ->name('pieChartTest')
            ->type('pie')
            ->size(['width' => 200, 'height' => 100])
            ->labels(['ISSUED', 'NOT ISSUED'])
            ->datasets([
                [
                    'backgroundColor' => ['#FF6384', '#36A2EB'],
                    'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                    'data' => [$issued_count,$unissued_count]
                ]
            ])
            ->options([]);
                        

         return view('chequebk.ibank',['ibank_chqbks' => $ibank_chqbks,
                                        'branches'=>$branches,
                                        'chartjs'=>$chartjs]);
    }

      /**
     * Display a listing of the Cbank chequebook request.
     *
     * @return \Illuminate\Http\Response
     */
     public function cbank_trans()
    {
        //
        $today = strtoupper(date('Y-m-d'));

         $branches = DB::table('tb_branch')
                        ->where('br_code','>','0')
                        ->get();

         $ibank_chqbks = DB::table('VW_DASHBOARD_CHEQUEBOOK')
                            ->where([
                                ['CHANNEL','INTBGATEWAY'],
                                ['POSTING_DATE','>=',$today],
                                ['TYPE_OF_ACCT','C']
                            ])
                            ->orderBy('POSTING_DATE','desc')
                            ->get();

         $issued_count = DB::table('VW_DASHBOARD_CHEQUEBOOK')
                             ->where([
                                ['CHANNEL','INTBGATEWAY'],
                                ['POSTING_DATE','>=',$today],
                                ['TYPE_OF_ACCT','C'],
                                ['FLAG','I']
                            ])
                            ->count();

         $unissued_count =  DB::table('VW_DASHBOARD_CHEQUEBOOK')
                             ->where([
                                ['CHANNEL','INTBGATEWAY'],
                                ['POSTING_DATE','>=',$today],
                                ['TYPE_OF_ACCT','C'],
                                ['FLAG','<>','I']
                            ])
                            ->count();

           
          $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['ISSUED', 'NOT ISSUED'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$issued_count,$unissued_count]
            ]
        ])
        ->options([]);
                        

         return view('chequebk.cbank',['ibank_chqbks' => $ibank_chqbks,
                                        'branches'=>$branches,
                                        'chartjs'=>$chartjs]);
    }
    /**
     * Search listing of the cbank chequebook request per search parameters.
     *
     * @return \Illuminate\Http\Response
     */
     public function cbank_search()
    {
       
        //
         $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $branch = $inputs['branch'];

         $branches = DB::table('tb_branch')
                        ->where('br_code','>','0')
                        ->get();

         $ibank_chqbks = DB::table('VW_DASHBOARD_CHEQUEBOOK')
                            ->where([
                                ['CHANNEL','INTBGATEWAY'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['TYPE_OF_ACCT','C']
                                 ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->get();

         $issued_count = DB::table('VW_DASHBOARD_CHEQUEBOOK')
                             ->where([
                                ['CHANNEL','INTBGATEWAY'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['TYPE_OF_ACCT','C'],
                                ['FLAG','I']
                                 ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

         $unissued_count =  DB::table('VW_DASHBOARD_CHEQUEBOOK')
                             ->where([
                                ['CHANNEL','INTBGATEWAY'],
                                ['DELIVERY_BRANCH','like','%'.$branch.'%'],
                                ['TYPE_OF_ACCT','C'],
                                ['FLAG','<>','I']
                                 ])
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->count();

           
          $chartjs = app()->chartjs
            ->name('pieChartTest')
            ->type('pie')
            ->size(['width' => 200, 'height' => 100])
            ->labels(['ISSUED', 'NOT ISSUED'])
            ->datasets([
                [
                    'backgroundColor' => ['#FF6384', '#36A2EB'],
                    'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                    'data' => [$issued_count,$unissued_count]
                ]
            ])
            ->options([]);
                        

         return view('chequebk.cbank',['ibank_chqbks' => $ibank_chqbks,
                                        'branches'=>$branches,
                                        'chartjs'=>$chartjs]);
    }


    

}
