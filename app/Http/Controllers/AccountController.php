<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Requester;

use App\Http\Requests;
use Datatables;
use DB;
use Request;
use Session;
use App\User;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
   /**
     * Display a listing of the mbank statement request.
     *
     * @return \Illuminate\Http\Response
     */
     public function mbank_trans()
    {
        //
        $today = strtoupper(date('Y-m-d'));

        $branches = DB::table('tb_branch')
                        ->where('br_code','>','0')
                        ->get();

         $mbank_stmts = DB::table('vw_dashboard_acct')
                            ->where([
                                ['CHANNEL','MOB'],
                                ['C_TYPE','I'],
                                ['DATE_OPENED',$today]
                            ])                            
                            ->get();
        // echo $mbank_stmts; die;
        $ordinary_count = DB::table('vw_dashboard_acct')
                            ->where([
                                ['TYPE_OF_ACCT','1'],
                                 ['CHANNEL','MOB'],
                                 ['C_TYPE','I'],
                                 ['DATE_OPENED',$today]
                            ])
                            ->count();

        $visa_count = DB::table('vw_dashboard_acct')
                            ->where([
                                ['TYPE_OF_ACCT','2'],
                                ['CHANNEL','MOB'],
                                ['C_TYPE','I'],
                                ['DATE_OPENED',$today]
                            ])
                            ->count();

        // echo $mbank_stmts; die;

       $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Current Account', 'Savings Account'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$ordinary_count,  $visa_count]
            ]
        ])
        ->options([]);



         return view('account.mbank',['mbank_stmts' => $mbank_stmts,
                                        'chartjs'=>$chartjs]);
    }

     public function mbank_search()
    {

        //

        $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $accttype = $inputs['accttype'];

        // echo $startdate; die;


         $mbank_stmts = DB::table('vw_dashboard_acct')
                            ->where([
                               ['CHANNEL','MOB'],
                               ['C_TYPE','I'],
                               ['TYPE_OF_ACCT','like','%'.$accttype.'%']                          
                            ])
                            ->whereBetween('DATE_OPENED', [$startdate, $enddate])
                            // ->orderBy('POSTING_DATE','desc')
                            ->get();

        // echo $mbank_stmts; die;

        $ordinary_count = DB::table('vw_dashboard_acct')
                            ->where([
                                  ['CHANNEL','MOB'],
                                  ['C_TYPE','I'],
                               ['TYPE_OF_ACCT','1']         
                            ])
                            ->whereBetween('DATE_OPENED', [$startdate, $enddate])
                            ->count();

        $visa_count = DB::table('vw_dashboard_acct')
                            ->where([
                                  ['CHANNEL','MOB'],
                                  ['C_TYPE','I'],
                               ['TYPE_OF_ACCT','2']         
                            ])
                            ->whereBetween('DATE_OPENED', [$startdate, $enddate])
                            ->count();



       $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Current Account', 'Savings Account'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$ordinary_count,  $visa_count]
            ]
        ])
        ->options([]);



         return view('account.mbank',['mbank_stmts' => $mbank_stmts,
                                        'chartjs'=>$chartjs]);
    }

      public function ibank_trans()
    {
        //
        $today = strtoupper(date('Y-m-d'));

        $branches = DB::table('tb_branch')
                        ->where('br_code','>','0')
                        ->get();

         $mbank_stmts = DB::table('vw_dashboard_acct')
                            ->where([
                                ['CHANNEL','NET'],
                                ['C_TYPE','I'],
                                ['DATE_OPENED',$today]
                            ])                            
                            ->get();
        // echo $mbank_stmts; die;
        $ordinary_count = DB::table('vw_dashboard_acct')
                            ->where([
                                ['TYPE_OF_ACCT','1'],
                                 ['CHANNEL','NET'],
                                 ['C_TYPE','I'],
                                 ['DATE_OPENED',$today]
                            ])
                            ->count();

        $visa_count = DB::table('vw_dashboard_acct')
                            ->where([
                                ['TYPE_OF_ACCT','2'],
                                 ['CHANNEL','NET'],
                                 ['C_TYPE','I'],
                                 ['DATE_OPENED',$today]
                            ])
                            ->count();

        // echo $mbank_stmts; die;

       $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Current Account', 'Savings Account'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$ordinary_count,  $visa_count]
            ]
        ])
        ->options([]);



         return view('account.ibank',['ibank_stmts' => $mbank_stmts,
                                        'chartjs'=>$chartjs]);
    }

     public function ibank_search()
    {

        //

        $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];
        $accttype = $inputs['accttype'];

        // echo $startdate; die;


         $mbank_stmts = DB::table('vw_dashboard_acct')
                            ->where([
                               ['CHANNEL','NET'],
                               ['TYPE_OF_ACCT','like','%'.$accttype.'%']                          
                            ])
                            ->whereBetween('DATE_OPENED', [$startdate, $enddate])
                            // ->orderBy('POSTING_DATE','desc')
                            ->get();

        // echo $mbank_stmts; die;

        $ordinary_count = DB::table('vw_dashboard_acct')
                            ->where([
                                  ['CHANNEL','NET'],
                               ['TYPE_OF_ACCT','1']         
                            ])
                            ->whereBetween('DATE_OPENED', [$startdate, $enddate])
                            ->count();

        $visa_count = DB::table('vw_dashboard_acct')
                            ->where([
                                  ['CHANNEL','NET'],
                               ['TYPE_OF_ACCT','2']          
                            ])
                            ->whereBetween('DATE_OPENED', [$startdate, $enddate])
                            ->count();



       $chartjs = app()->chartjs
        ->name('pieChartTest')
        ->type('pie')
        ->size(['width' => 200, 'height' => 100])
        ->labels(['Current Account', 'Savings Account'])
        ->datasets([
            [
                'backgroundColor' => ['#FF6384', '#36A2EB'],
                'hoverBackgroundColor' => ['#FF6384', '#36A2EB'],
                'data' => [$ordinary_count,  $visa_count]
            ]
        ])
        ->options([]);



         return view('account.ibank',['ibank_stmts' => $mbank_stmts,
                                        'chartjs'=>$chartjs]);
    }

}
