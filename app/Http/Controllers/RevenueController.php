<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as Requester;

use Datatables;
use DB;
use Request;
use Session;

class RevenueController extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mbankindex()
    {
        //
        $today = strtoupper(date('Y-m-d'));     

        $mbanksum = DB::table('VW_DASHBOARD_REVENUE')
                            ->select('DESCRIPTION',DB::raw('SUM(REVENUE) as REVENUE,SUM(TAX) as TAX,sum(REVENUE)+sum(TAX) as total'))
                            ->where([
                                ['channel','MBANK'],
                                ['POSTING_DATE','>=',$today]
                            ])
                            ->groupBy('DESCRIPTION')
                            ->get();
        // echo $mbanksum; die;

        return view('revenue.mbank',['mbank_revenues' => $mbanksum]);
    }

      public function mbanksearch()
    {
        //
         $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];

        $date = 'Date between '.$startdate.' - '.$enddate;


          $mbanksum = DB::table('VW_DASHBOARD_REVENUE')
                            ->select('DESCRIPTION',DB::raw('SUM(REVENUE) as REVENUE,SUM(TAX) as TAX,sum(REVENUE)+sum(TAX) as total'))
                            ->where('channel','MBANK')
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->groupBy('DESCRIPTION')
                            ->get();
        // echo $mbanksum; die;
        return view('revenue.mbank',['mbank_revenues' => $mbanksum]);

    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ibankindex()
    {
        //
        $today = strtoupper(date('Y-m-d'));

     

        $ibanksum = DB::table('VW_DASHBOARD_REVENUE')
                            ->select('DESCRIPTION',DB::raw('SUM(REVENUE) as REVENUE,SUM(TAX) as TAX,sum(REVENUE)+sum(TAX) as total'))
                            ->where([
                                ['channel','NETBANK'],
                                ['POSTING_DATE','>=',$today]
                                ])
                            ->groupBy('DESCRIPTION')
                            ->get();
        // echo $ibanksum; die;
        return view('revenue.ibank',['ibank_revenues' => $ibanksum]);
    }

      public function ibanksearch()
    {
        //
         $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];

        $date = 'Date between '.$startdate.' - '.$enddate;


          $ibanksum = DB::table('VW_DASHBOARD_REVENUE')
                            ->select('DESCRIPTION',DB::raw('SUM(REVENUE) as REVENUE,SUM(TAX) as TAX,sum(REVENUE)+sum(TAX) as total'))
                            ->where('channel','NETBANK')
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->groupBy('DESCRIPTION')
                            ->get();
        // echo $ibanksum; die;
        return view('revenue.ibank',['ibank_revenues' => $ibanksum]);

    }


 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function mtellerindex()
    {
        //
        $today = strtoupper(date('Y-m-d'));

     

        $ibanksum = DB::table('VW_DASHBOARD_REVENUE')
                            ->select('DESCRIPTION',DB::raw('SUM(REVENUE) as REVENUE,SUM(TAX) as TAX,sum(REVENUE)+sum(TAX) as total'))
                            ->where([
                                ['channel','MTELLER'],
                                ['POSTING_DATE','>=',$today]
                                ])
                            ->groupBy('DESCRIPTION')
                            ->get();
        // echo $ibanksum; die;
        return view('revenue.mteller',['mteller_revenues' => $ibanksum]);
    }

      public function mtellersearch()
    {
        //
         $inputs = Request::all();

        $startdate = $inputs['startdate'];
        $enddate = $inputs['enddate'];

        $date = 'Date between '.$startdate.' - '.$enddate;


          $ibanksum = DB::table('VW_DASHBOARD_REVENUE')
                            ->select('DESCRIPTION',DB::raw('SUM(REVENUE) as REVENUE,SUM(TAX) as TAX,sum(REVENUE)+sum(TAX) as total'))
                            ->where('channel','MTELLER')
                            ->whereBetween('POSTING_DATE', [$startdate, $enddate])
                            ->groupBy('DESCRIPTION')
                            ->get();
        // echo $ibanksum; die;
        return view('revenue.mteller',['mteller_revenues' => $ibanksum]);

    }

}
