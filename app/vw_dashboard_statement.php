<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vw_dashboard_statement extends Model
{
    //
      protected $table = 'vw_dashboard_statement';
    protected $primaryKey = 'ROWNUM';
    public $incrementing = false;
    protected $keyType = String;
    public $timestamps = false;
}
