@extends('layouts.master')

@section('title')
 Dashboard
@endsection

@section('header')
  <h1 class="page-title">Dashboard</h1>
@endsection

@section('content')
            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-md-6 animatedParent animateOnce z-index-50">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Mobile Banking Login</div> 
                                    <ul class="panel-tool-options"> 
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li>
                                    </ul> 
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body">
                                    <div class="row col-with-divider">
                                        @foreach ($mbank_todays as $mbank_today)
                                            <div class="col-xs-6 text-center stack-order">                        
                                                <h1 class="no-margins">{{$mbank_today ? $mbank_today->count : '0'}}</h1>
                                                <small>{{$mbank_today->dates}}</small>
                                            </div>
                                        @endforeach
                                       <!--  <div class="col-xs-6 text-center stack-order"> 
                                           
                                            <h1 class="no-margins">55</h1>
                                            <small>Today</small>
                                        </div> -->
                                    </div>
                                </div> 
                            </div>
                        </div>

                        <div class="col-md-6 animatedParent animateOnce z-index-50">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Internet Banking Logins</div> 
                                    <ul class="panel-tool-options"> 
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li>
                                    </ul> 
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body">
                                    <div class="row col-with-divider">
                                        @foreach ($netbank_todays as $netbank_today)
                                            <div class="col-xs-6 text-center stack-order">                        
                                                <h1 class="no-margins">{{$netbank_today ? $netbank_today->count : '0'}}</h1>
                                                <small>{{$netbank_today->dates}}</small>
                                            </div>
                                        @endforeach
                                    </div>
                                </div> 
                            </div>
                        </div>                      
                    </div>

                    <div class="row">
                        <div class="col-md-6 animatedParent animateOnce z-index-50">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Signups</div> 
                                    <ul class="panel-tool-options"> 
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li>
                                    </ul> 
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body">
                                    <div class="row col-with-divider">
                                        @foreach ($signup_sums as $signup_sum)
                                            <div class="col-xs-6 text-center stack-order">                        
                                                <h1 class="no-margins">{{$signup_sum ? $signup_sum->t_count : '0'}}</h1>
                                                <small>{{$signup_sum->signup_date}}</small>
                                            </div>
                                        @endforeach
                                       <!--  <div class="col-xs-6 text-center stack-order"> 
                                           
                                            <h1 class="no-margins">55</h1>
                                            <small>Today</small>
                                        </div> -->
                                    </div>
                                </div> 
                            </div>
                        </div>

                        <div class="col-md-6 animatedParent animateOnce z-index-50">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Locked Users</div> 
                                    <ul class="panel-tool-options"> 
                                       <!--  <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li> -->
                                    </ul> 
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body">
                                    <div class="row col-with-divider">
                                         @foreach ($locks as $lock)
                                            <div class="col-xs-6 text-center stack-order">                        
                                                <h1 class="no-margins">{{$lock ? $lock->lock_count : '0'}}</h1>
                                                <small>{{$lock->lock_type}}</small>
                                            </div>
                                        @endforeach
                                    </div>
                                </div> 
                            </div>
                        </div>                      
                    </div>

                    <div class="row">
                        <div class="col-md-6 animatedParent animateOnce z-index-50">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Users Status</div> 
                                   <!--  <ul class="panel-tool-options"> 
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li>
                                    </ul>  -->
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body">
                                    <div class="row col-with-divider">
                                        @foreach ($active_users as $active_user)
                                            <div class="col-xs-6 text-center stack-order">                        
                                                <h1 class="no-margins">{{$active_user ? $active_user->val : '0'}}</h1>
                                                <small>{{$active_user->status}}</small>
                                            </div>
                                        @endforeach
                                       <!--  <div class="col-xs-6 text-center stack-order"> 
                                           
                                            <h1 class="no-margins">55</h1>
                                            <small>Today</small>
                                        </div> -->
                                    </div>
                                </div> 
                            </div>
                        </div>

                        <div class="col-md-6 animatedParent animateOnce z-index-50">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Locked Users</div> 
                                    <ul class="panel-tool-options"> 
                                       <!--  <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li> -->
                                    </ul> 
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body">
                                    <div class="row col-with-divider">
                                         @foreach ($locks as $lock)
                                            <div class="col-xs-6 text-center stack-order">                        
                                                <h1 class="no-margins">{{$lock ? $lock->lock_count : '0'}}</h1>
                                                <small>{{$lock->lock_type}}</small>
                                            </div>
                                        @endforeach
                                    </div>
                                </div> 
                            </div>
                        </div>                      
                    </div>

                    <div class="row">
                          <div class="col-md-6 animatedParent animateOnce z-index-49">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Internet Banking Today</div> 
                                    <ul class="panel-tool-options"> 
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li>
                                    </ul>  
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body"> 
                                    @foreach ($netbank_totals as $netbank_total)
                                    <div class="stack-order">
                                        <h1 class="no-margins" style="color: blue;font-weight: bold">{{number_format($netbank_total->total,2, '.', ',')}}</h1>
                                        <small>FROM <span style="color: blue;font-weight: bold">{{$netbank_total->counts}} </span>{{$netbank_total->voucher_number}} TRANSACTIONS.</small>
                                    </div>
                                    <hr/>
                                    @endforeach
                                   
                                </div> 
                            </div>
                        </div>

                          <div class="col-md-6 animatedParent animateOnce z-index-49">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Mobile Banking Today</div> 
                                    <ul class="panel-tool-options"> 
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li>
                                    </ul>  
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body"> 
                                     @foreach ($mbank_totals as $mbank_total)
                                    <div class="stack-order">
                                        <h1 class="no-margins" style="color: blue;font-weight: bold">{{number_format($mbank_total->total,2,'.',',')}}</h1>
                                        <small>FROM <span style="color: blue;font-weight: bold">{{$mbank_total->counts}}</span> {{$mbank_total->voucher_number}} TRANSACTIONS.</small>
                                    </div>
                                    <hr/>
                                    @endforeach
                                </div> 
                            </div>
                        </div>
                        
                    </div>                   
                </div>

                <div class="col-lg-6">
                    <div class="animatedParent animateOnce z-index-44">
                        <div class="panel-group animated fadeInUp">
                            <div class="panel panel-invert">
                                <div class="panel-heading no-border clearfix"> 
                                    <h2 class="panel-title">Revenue</h2>
                                   <!--  <ul class="panel-tool-options"> 
                                        <li><a href="#" id="Revenuelines"><i class="icon-chart-line icon-2x"></i></a></li>
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog icon-2x"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li>
                                    </ul>  -->
                                </div>
                                <div class="panel-body">
                                     {!! $chartjs->render() !!}
                                   <!--  <div class="canvas-chart">
                                        <canvas id="pieChart" height="140"></canvas>
                                    </div>   -->
                                   <!--  <div class="flot-chart">
                                        <div id="Revenue-lines" class="flot-chart-content"></div>
                                    </div> -->
                                    <div id="placeholder_overview" style="width:100%; height:60px;"></div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="panel-update-content">
                                        <div class="row-revenue clearfix">
                                            @foreach ($revenues as $revenue)
                                            <div class="col-xs-4">
                                                <h5>{{$revenue->channel}}</h5>
                                                <h1>{{number_format($revenue->revenue,2, '.', ',')}}</h1>
                                            </div>
                                            @endforeach<!-- 
                                            <div class="col-xs-6">
                                                <h5>Net Revenue</h5>
                                                <h1>6,734.89</h1>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                                        
                </div>
            </div>
            
           <!--  <div class="row">
                <div class="col-lg-12 animatedParent animateOnce z-index-38">
                    <div class="panel panel-default animated fadeInUp">
                        <div class="panel-heading no-border clearfix"> 
                            <h2 class="panel-title">Site Traffic</h2>
                            <ul class="panel-tool-options"> 
                                <li><a href="#" id="lines"><i class="icon-chart-line icon-2x"></i></a></li>
                                <li><a href="#" id="bars"><i class="icon-chart-bar icon-2x"></i></a></li>
                                <li class="dropdown">
                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog icon-2x"></i></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                        <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                        <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                    </ul>
                                 </li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="flot-chart float-chart-lg">
                                <div id="graph-lines" class="flot-chart-content"></div>
                                <div id="graph-bars" class="flot-chart-content"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        

@endsection

@section('scripts')

<!--Float Charts-->
<!-- <script src="{{asset('js/plugins/flot/jquery.flot.min.js')}}"></script>
<script src="{{asset('js/plugins/flot/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{asset('js/plugins/flot/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('js/plugins/flot/jquery.flot.selection.min.js')}}"></script>        
<script src="{{asset('js/plugins/flot/jquery.flot.pie.min.js')}}"></script>
<script src="{{asset('js/plugins/flot/jquery.flot.time.min.js')}}"></script> -->

<!-- <script src="{{asset('js/plugins/chartjs/chartjs-script.js')}}"></script> -->


<script type="text/javascript">
    $('#dashboard').addClass('active');
</script>

@endsection