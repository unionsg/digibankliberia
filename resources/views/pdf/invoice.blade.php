
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.g-axon.com/mouldifi-5.0/light/blank-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Feb 2018 09:01:59 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- <meta http-equiv="refresh" content="300"/> -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Mouldifi - A fully responsive, HTML5 based admin theme">
<meta name="keywords" content="Responsive, HTML5, admin theme, business, professional, Mouldifi, web design, CSS3">
<!-- Site favicon -->
<link rel='shortcut icon' type='image/x-icon' href='images/favicon.ico' />
<!-- /site favicon -->

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

<!-- Entypo font stylesheet -->
<link href="{{ asset('css/entypo.css') }}" rel="stylesheet">
<!-- /entypo font stylesheet -->

<!-- Font awesome stylesheet -->
<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
<!-- /font awesome stylesheet -->

<!-- CSS3 Animate It Plugin Stylesheet -->
<link href="{{asset('css/plugins/css3-animate-it-plugin/animations.css') }}" rel="stylesheet">
<!-- /css3 animate it plugin stylesheet -->

<!-- Bootstrap stylesheet min version -->
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- /bootstrap stylesheet min version -->

<!-- Mouldifi core stylesheet -->
<link href="{{asset('css/mouldifi-core.css')}}" rel="stylesheet">
<!-- /mouldifi core stylesheet -->

<link href="{{asset('css/mouldifi-forms.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/select2/select2.css')}}" rel="stylesheet">
<!-- Bootstrap stylesheet min version -->
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- /bootstrap stylesheet min version -->

</head>
<body>
            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-md-6 animatedParent animateOnce z-index-50">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Mobile Banking Login</div> 
                                    <ul class="panel-tool-options"> 
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li>
                                    </ul> 
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body">
                                    <div class="row col-with-divider">
                                        @foreach ($mbank_todays as $mbank_today)
                                            <div class="col-xs-6 text-center stack-order">                        
                                                <h1 class="no-margins">{{$mbank_today ? $mbank_today->count : '0'}}</h1>
                                                <small>{{$mbank_today->dates}}</small>
                                            </div>
                                        @endforeach
                                       <!--  <div class="col-xs-6 text-center stack-order"> 
                                           
                                            <h1 class="no-margins">55</h1>
                                            <small>Today</small>
                                        </div> -->
                                    </div>
                                </div> 
                            </div>
                        </div>

                        <div class="col-md-6 animatedParent animateOnce z-index-50">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Internet Banking Logins</div> 
                                    <ul class="panel-tool-options"> 
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li>
                                    </ul> 
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body">
                                    <div class="row col-with-divider">
                                        @foreach ($netbank_todays as $netbank_today)
                                            <div class="col-xs-6 text-center stack-order">                        
                                                <h1 class="no-margins">{{$netbank_today ? $netbank_today->count : '0'}}</h1>
                                                <small>{{$netbank_today->dates}}</small>
                                            </div>
                                        @endforeach
                                    </div>
                                </div> 
                            </div>
                        </div>                      
                    </div>

                    <div class="row">
                        <div class="col-md-6 animatedParent animateOnce z-index-50">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Signups</div> 
                                    <ul class="panel-tool-options"> 
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li>
                                    </ul> 
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body">
                                    <div class="row col-with-divider">
                                        @foreach ($signup_sums as $signup_sum)
                                            <div class="col-xs-6 text-center stack-order">                        
                                                <h1 class="no-margins">{{$signup_sum ? $signup_sum->t_count : '0'}}</h1>
                                                <small>{{$signup_sum->signup_date}}</small>
                                            </div>
                                        @endforeach
                                       <!--  <div class="col-xs-6 text-center stack-order"> 
                                           
                                            <h1 class="no-margins">55</h1>
                                            <small>Today</small>
                                        </div> -->
                                    </div>
                                </div> 
                            </div>
                        </div>

                        <div class="col-md-6 animatedParent animateOnce z-index-50">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Locked Users</div> 
                                    <ul class="panel-tool-options"> 
                                       <!--  <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li> -->
                                    </ul> 
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body">
                                    <div class="row col-with-divider">
                                         @foreach ($locks as $lock)
                                            <div class="col-xs-6 text-center stack-order">                        
                                                <h1 class="no-margins">{{$lock ? $lock->lock_count : '0'}}</h1>
                                                <small>{{$lock->lock_type}}</small>
                                            </div>
                                        @endforeach
                                    </div>
                                </div> 
                            </div>
                        </div>                      
                    </div>

                    <div class="row">
                        <div class="col-md-6 animatedParent animateOnce z-index-50">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Users Status</div> 
                                   <!--  <ul class="panel-tool-options"> 
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li>
                                    </ul>  -->
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body">
                                    <div class="row col-with-divider">
                                        @foreach ($active_users as $active_user)
                                            <div class="col-xs-6 text-center stack-order">                        
                                                <h1 class="no-margins">{{$active_user ? $active_user->val : '0'}}</h1>
                                                <small>{{$active_user->status}}</small>
                                            </div>
                                        @endforeach
                                       <!--  <div class="col-xs-6 text-center stack-order"> 
                                           
                                            <h1 class="no-margins">55</h1>
                                            <small>Today</small>
                                        </div> -->
                                    </div>
                                </div> 
                            </div>
                        </div>

                        <div class="col-md-6 animatedParent animateOnce z-index-50">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Locked Users</div> 
                                    <ul class="panel-tool-options"> 
                                       <!--  <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li> -->
                                    </ul> 
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body">
                                    <div class="row col-with-divider">
                                         @foreach ($locks as $lock)
                                            <div class="col-xs-6 text-center stack-order">                        
                                                <h1 class="no-margins">{{$lock ? $lock->lock_count : '0'}}</h1>
                                                <small>{{$lock->lock_type}}</small>
                                            </div>
                                        @endforeach
                                    </div>
                                </div> 
                            </div>
                        </div>                      
                    </div>

                    <div class="row">
                          <div class="col-md-6 animatedParent animateOnce z-index-49">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Internet Banking Today</div> 
                                    <ul class="panel-tool-options"> 
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li>
                                    </ul>  
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body"> 
                                    @foreach ($netbank_totals as $netbank_total)
                                    <div class="stack-order">
                                        <h1 class="no-margins" style="color: blue;font-weight: bold">{{number_format($netbank_total->total,2, '.', ',')}}</h1>
                                        <small>FROM <span style="color: blue;font-weight: bold">{{$netbank_total->counts}} </span>{{$netbank_total->voucher_number}} TRANSACTIONS.</small>
                                    </div>
                                    <hr/>
                                    @endforeach
                                   
                                </div> 
                            </div>
                        </div>

                          <div class="col-md-6 animatedParent animateOnce z-index-49">
                            <div class="panel minimal panel-default animated fadeInUp">
                                <div class="panel-heading clearfix"> 
                                    <div class="panel-title">Mobile Banking Today</div> 
                                    <ul class="panel-tool-options"> 
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li>
                                    </ul>  
                                </div> 
                                <!-- panel body --> 
                                <div class="panel-body"> 
                                     @foreach ($mbank_totals as $mbank_total)
                                    <div class="stack-order">
                                        <h1 class="no-margins" style="color: blue;font-weight: bold">{{number_format($mbank_total->total,2,'.',',')}}</h1>
                                        <small>FROM <span style="color: blue;font-weight: bold">{{$mbank_total->counts}}</span> {{$mbank_total->voucher_number}} TRANSACTIONS.</small>
                                    </div>
                                    <hr/>
                                    @endforeach
                                </div> 
                            </div>
                        </div>
                        
                    </div>                   
                </div>

                <div class="col-lg-6">
                    <div class="animatedParent animateOnce z-index-44">
                        <div class="panel-group animated fadeInUp">
                            <div class="panel panel-invert">
                                <div class="panel-heading no-border clearfix"> 
                                    <h2 class="panel-title">Revenue</h2>
                                   <!--  <ul class="panel-tool-options"> 
                                        <li><a href="#" id="Revenuelines"><i class="icon-chart-line icon-2x"></i></a></li>
                                        <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false"><i class="icon-cog icon-2x"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-arrows-ccw"></i> Update data</a></li>
                                                <li><a href="#"><i class="icon-list"></i> Detailed log</a></li>
                                                <li><a href="#"><i class="icon-chart-pie"></i> Statistics</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#"><i class="icon-cancel"></i> Clear list</a></li>
                                            </ul>
                                         </li>
                                    </ul>  -->
                                </div>
                                <div class="panel-body">
                                     {!! $chartjs->render() !!}
                                   <!--  <div class="canvas-chart">
                                        <canvas id="pieChart" height="140"></canvas>
                                    </div>   -->
                                   <!--  <div class="flot-chart">
                                        <div id="Revenue-lines" class="flot-chart-content"></div>
                                    </div> -->
                                    <div id="placeholder_overview" style="width:100%; height:60px;"></div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-body">
                                    <div class="panel-update-content">
                                        <div class="row-revenue clearfix">
                                            @foreach ($revenues as $revenue)
                                            <div class="col-xs-6">
                                                <h5>{{$revenue->channel}}</h5>
                                                <h1>{{$revenue->revenue}}</h1>
                                            </div>
                                            @endforeach<!-- 
                                            <div class="col-xs-6">
                                                <h5>Net Revenue</h5>
                                                <h1>6,734.89</h1>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                                        
                </div>
            </div>


<!--Load JQuery-->
<script src="{{asset('js/jquery.min.js')}}"></script>
<!-- Load CSS3 Animate It Plugin JS -->
<script src="{{asset('js/plugins/css3-animate-it-plugin/css3-animate-it.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugins/metismenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('js/plugins/blockui-master/jquery-ui.js')}}"></script>
<script src="{{asset('js/plugins/blockui-master/jquery.blockUI.js')}}"></script>
<script src="{{asset('js/functions.js')}}"></script>
<!--Bootstrap DatePicker-->
<script src="{{asset('js/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- Select2-->
<script src="{{asset('js/plugins/select2/select2.full.min.js')}}"></script>
<!--ChartJs-->
<script src="{{asset('js/plugins/chartjs/Chart.js')}}"></script>

@yield('scripts')
</body>

<!-- Mirrored from www.g-axon.com/mouldifi-5.0/light/blank-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Feb 2018 09:01:59 GMT -->
</html>
