
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.g-axon.com/mouldifi-5.0/light/blank-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Feb 2018 09:01:59 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- <meta http-equiv="refresh" content="300"/> -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Mouldifi - A fully responsive, HTML5 based admin theme">
<meta name="keywords" content="Responsive, HTML5, admin theme, business, professional, Mouldifi, web design, CSS3">
<title>{{ config('app.name', 'Laravel') }} | @yield('title')</title>
<!-- Site favicon -->
<link rel='shortcut icon' type='image/x-icon' href='images/favicon.ico' />
<!-- /site favicon -->

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">

<!-- Entypo font stylesheet -->
<link href="{{ asset('css/entypo.css') }}" rel="stylesheet">
<!-- /entypo font stylesheet -->

<!-- Font awesome stylesheet -->
<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
<!-- /font awesome stylesheet -->

<!-- CSS3 Animate It Plugin Stylesheet -->
<link href="{{asset('css/plugins/css3-animate-it-plugin/animations.css') }}" rel="stylesheet">
<!-- /css3 animate it plugin stylesheet -->

<!-- Bootstrap stylesheet min version -->
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
<!-- /bootstrap stylesheet min version -->

<!-- Mouldifi core stylesheet -->
<link href="{{asset('css/mouldifi-core.css')}}" rel="stylesheet">
<!-- /mouldifi core stylesheet -->

<link href="{{asset('css/mouldifi-forms.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/select2/select2.css')}}" rel="stylesheet">
<link href="{{asset('css/plugins/datepicker/bootstrap-datepicker.css')}}" rel="stylesheet">

@yield('styles')

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
<![endif]-->

 <!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>

<!-- Page container -->
<div class="page-container">

  <!-- Page Sidebar -->
  <div class="page-sidebar">
  
  		<!-- Site header  -->
		<header class="site-header">
		  <div class="site-logo"><a href="{{url('/home')}}"><img src="images/logo.png" alt="digibank" title="digibank"></a></div>
		  <div class="sidebar-collapse hidden-xs"><a class="sidebar-collapse-icon" href="#"><i class="icon-menu"></i></a></div>
		  <div class="sidebar-mobile-menu visible-xs"><a data-target="#side-nav" data-toggle="collapse" class="mobile-menu-icon" href="#"><i class="icon-menu"></i></a></div>
		</header>
		<!-- /site header -->
		
		<!-- Main navigation -->
		<ul id="side-nav" class="main-menu navbar-collapse collapse">			
			<li id="dashboard"><a href="{{ url('/home') }}"><i class="icon-gauge"></i><span class="title">Dashboard</span></a></li>
			<li class="has-sub" id="users"><a href="collapsed-sidebar.html"><i class="icon-layout"></i><span class="title">Users</span></a>
				<ul class="nav collapse" id="users-collapse">
					<li id="reg-users"><a href="{{url('/reg-users')}}"><span class="title">Registered Users</span></a></li>
					<li id="lck-users"><a href="{{url('/lk-users')}}"><span class="title">Locked Users</span></a></li>
					<li id="blck-users"><a href="{{url('/blk-users')}}"><span class="title">Blocked Users</span></a></li>
					<li id="act-users"><a href="{{url('/active-users')}}"><span class="title">Active/Inactive Users</span></a></li>
				</ul>
			</li>
			<li class="has-sub" id="ibank"><a href="panels.html"><i class="icon-newspaper"></i><span class="title">Internet Banking</span></a>
				<ul class="nav collapse" id="ibank-collapse">
					<li class="has-sub" id="ib-login">
						<a href="#/"><span class="title">Login Statistics</span></a>
						<ul class="nav collapse" id="ibanklogin-subcollapse"> 
							<li id="ibank-loginsum"><a href="{{url('/ibank-loginsum')}}"><span class="title">Summary</span></a></li> 
							<li id="ibank-logindetail"><a href="{{url('/ibank-logindetails')}}"><span class="title">Details</span></a></li> 
						</ul> 
					</li>		
					<li class="has-sub" id="ib-ftrans">
						<a href="#/"><span class="title">Funds Transfer</span></a> 
						<ul class="nav collapse" id="ibank-sub-collapse"> 							
							<!-- <li id="ibank-slink"><a href="{{url('/ibank-slink')}}"><span class="title">Salone Link</span></a></li>  -->
							<li id="ibank-itrans"><a href="{{url('/ibank-int-transfer')}}"><span class="title">Internal Transfer</span></a></li>
						</ul> 
					</li> 
					<li id="ib-dcredit"><a href="{{url('/ibank-ach')}}"><span class="title">Direct Credit</span></a></li>
					<!-- <li id="ib-blink"><a href="buttons.html"><span class="title">Blink Payments</span></a></li> -->
					<li id="ib-stmnt"><a href="{{url('/ibank-stmt')}}"><span class="title">Statement Request</span></a></li>
					<li id="ib-chkbk"><a href="{{url('/ibank-chqbk')}}"><span class="title">Cheque Book Request</span></a></li>
				    <!-- <li id="ib-acct"><a href="{{url('/ibank-acct')}}"><span class="title">Acount Opened</span></a></li> -->
				</ul>
			</li>
			<!-- <li class="has-sub" id="mbank"><a href=""><i class="icon-window"></i><span class="title">Mobile Banking</span></a>
				<ul class="nav collapse" id="mbank-collapse">
					<li class="has-sub" id="mb-login">
						<a href="#/"><span class="title">Login Statistics</span></a>
						<ul class="nav collapse" id="mbanklogin-subcollapse"> 
							<li id="mbank-loginsum"><a href="{{url('/mbank-loginsum')}}"><span class="title">Summary</span></a></li> 
							<li id="mbank-logindetail"><a href="{{url('/mbank-logindetails')}}"><span class="title">Details</span></a></li> 
						</ul> 
					</li>
					<li class="has-sub" id="mb-ftrans">
						<a href="#/"><span class="title">Funds Transfer</span></a> 
						<ul class="nav collapse" id="mbank-sub-collapse"> 
							<li id="mbank-slink"><a href="{{url('/mbank-slink')}}"><span class="title">Salone Link</span></a></li> 
							<li id="mbank-itrans"><a href="{{url('/mbank-int-transfer')}}"><span class="title">Internal Transfer</span></a></li> 
							<li id="mbank-qrtrans"><a href="{{url('/mbank-qr-transfer')}}"><span class="title">Quick Response(QR) Transfer</span></a></li> 
						</ul> 
					</li> 					
					<li id="mb-dcredit"><a href="{{url('/mbank-ach')}}"><span class="title">Direct Credit</span></a></li>
					<li id="mb-blink"><a href="basic-tables.html"><span class="title">Blink Payments</span></a></li>
					<li id="mb-stmnt"><a href="{{url('/mbank-stmt')}}"><span class="title">Statement Request</span></a></li>
					<li id="mb-chkbk"><a href="{{url('/mbank-chqbk')}}"><span class="title">Cheque Book Request</span></a></li>
					<li id="mb-acct"><a href="{{url('/mbank-acct')}}"><span class="title">Account Opened</span></a></li>
				</ul>
			</li>  -->
			<!-- <li class="has-sub" id="cbank"><a href="panels.html"><i class="icon-newspaper"></i><span class="title">Corporate Banking</span></a>
				<ul class="nav collapse" id="cbank-collapse">
					<li class="has-sub" id="cb-login">
						<a href="#/"><span class="title">Login Statistics</span></a>
						<ul class="nav collapse" id="cbanklogin-subcollapse"> 
							<li id="cbank-loginsum"><a href="{{url('/cbank-loginsum')}}"><span class="title">Summary</span></a></li> 
							<li id="cbank-logindetail"><a href="{{url('/cbank-logindetails')}}"><span class="title">Details</span></a></li> 
						</ul> 
					</li>		
					<li class="has-sub" id="cb-ftrans">
						<a href="#/"><span class="title">Funds Transfer</span></a> 
						<ul class="nav collapse" id="cbank-sub-collapse"> 							
							<li id="cbank-slink"><a href="{{url('/cbank-slink')}}"><span class="title">Salone Link</span></a></li> 
							<li id="cbank-itrans"><a href="{{url('/cbank-int-transfer')}}"><span class="title">Internal Transfer</span></a></li>
						</ul> 
					</li>
					<li id="cbank-itrans"><a href="{{url('/cbank-int-transfer')}}"><span class="title">Internal Transfer</span></a></li>
					<li id="cb-dcredit"><a href="{{url('/cbank-ach')}}"><span class="title">Direct Credit</span></a></li>
					<li id="ib-blink"><a href="buttons.html"><span class="title">Blink Payments</span></a></li>
					<li id="cb-stmnt"><a href="{{url('/cbank-stmt')}}"><span class="title">Statement Request</span></a></li>
					<li id="cb-chkbk"><a href="{{url('/cbank-chqbk')}}"><span class="title">Cheque Book Request</span></a></li>
				    <li id="cb-acct"><a href="{{url('/cbank-acct')}}"><span class="title">Acount Opened</span></a></li>
				</ul>
			</li> -->
			<li class="has-sub" id="revenue"><a href="basic-tables.html"><i class="icon-window"></i><span class="title">Revenues</span></a>
				<ul class="nav collapse" id="rev-collapse">
					<!-- <li id="mb-rev"><a href="{{url('/mbank-revenue')}}"><span class="title">Mobile Banking</span></a></li> -->
					<li id="ib-rev"><a href="{{url('/ibank-revenue')}}"><span class="title">Internet Banking</span></a></li>
					<!-- <li id="mt-rev"><a href="{{url('/mteller-revenue')}}"><span class="title">Mi Yone Teller</span></a></li> -->
				</ul>
			</li>
			<li class="has-sub" id="audit"><a href="basic-tables.html"><i class="icon-window"></i><span class="title">Audit Logs</span></a>
				<ul class="nav collapse" id="aud-collapse">
					<!-- <li id="mb-audit"><a href="{{url('/mbank-audit')}}"><span class="title">Mobile Banking</span></a></li> -->
					<li id="ib-audit"><a href="{{url('/ibank-audit')}}"><span class="title">Internet Banking</span></a></li>
				</ul>
			</li>				
			
		</ul>
		<!-- /main navigation -->		
  </div>
  <!-- /page sidebar -->
  
  <!-- Main container -->
  <div class="main-container gray-bg">
  
	<!-- Main header -->
    <div class="main-header row">
      <div class="col-sm-6 col-xs-7">
	   		@yield('header')
      </div>
	  
      <div class="col-sm-6 col-xs-5">
	  	<div class="pull-right">
			<!-- User Info -->
			<ul class="user-info pull-left">
			  <li class="profile-info dropdown">
				<a data-close-others="true" data-hover="dropdown" data-toggle="dropdown" class="dropdown-toggle" href="#">{{ Auth::user()->name }} <span class="caret"></span></a>
				<ul class="dropdown-menu pull-right">
					 <!-- <li><a href="#/"><i class="icon-user"></i>My profile</a></li>
		              <li><a href="#/"><i class="icon-mail"></i>Messages</a></li>
		              <li><a href="#"><i class="icon-clipboard"></i>Tasks</a></li>
					  <li class="divider"></li>
					  <li><a href="#"><i class="icon-cog"></i>Account settings</a></li> -->
					  <li><a href="{{url('/logout')}}"><i class="icon-logout"></i>Logout</a></li>
				</ul>
			  </li>
			</ul>
			<!-- /user info -->
			
		</div>
      </div>
    </div>
	<!-- /main header -->
	
	<!-- Main content -->
	<div class="main-content">
		@yield('content')
		
		<!-- Footer -->
		<footer class="animatedParent animateOnce z-index-10"> 
			<div class="footer-main animated fadeInUp slow">&copy; {{date('Y')}} <strong>Sierra Leone Commercial Bank</strong> Powered by <a target="_blank" href="https://www.unionsg.com">Union Systems Global</a></div> 
		</footer>	
		<!-- /footer -->
		
	  </div>
	  <!-- /main content -->
	  
  </div>
  <!-- /main container -->
  
</div>
<!-- /page container -->

<!--Load JQuery-->
<script src="{{asset('js/jquery.min.js')}}"></script>
<!-- Load CSS3 Animate It Plugin JS -->
<script src="{{asset('js/plugins/css3-animate-it-plugin/css3-animate-it.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugins/metismenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('js/plugins/blockui-master/jquery-ui.js')}}"></script>
<script src="{{asset('js/plugins/blockui-master/jquery.blockUI.js')}}"></script>
<script src="{{asset('js/functions.js')}}"></script>
<!--Bootstrap DatePicker-->
<script src="{{asset('js/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- Select2-->
<script src="{{asset('js/plugins/select2/select2.full.min.js')}}"></script>
<!--ChartJs-->
<script src="{{asset('js/plugins/chartjs/Chart.js')}}"></script>

@yield('scripts')
</body>

<!-- Mirrored from www.g-axon.com/mouldifi-5.0/light/blank-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 04 Feb 2018 09:01:59 GMT -->
</html>
