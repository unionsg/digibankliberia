@extends('layouts.master')

@section('styles')
<link href="{{asset('css/plugins/datatables/jquery.dataTables.css')}}" rel="stylesheet">
<link href="{{asset('js/plugins/datatables/extensions/Buttons/css/buttons.dataTables.css')}}" rel="stylesheet">

@endsection

@section('title')
 Mobile Banking - Account Opened
@endsection

@section('header')
  <h1 class="page-title">Mobile Banking - Account Opened</h1>
@endsection

@section('content')

<div class="row">
		<div class="col-lg-12 animatedParent animateOnce z-index-50">
			<div class="panel panel-default animated fadeInUp">				
				<div class="panel-body">
					<div class="row col-with-divider">
						<div class="col-md-6">
							<form class="form-horizontal" method="POST" action="{{url('mbank-acctsrch')}}"> 
								<input type="hidden" name="_token" value="{{csrf_token()}}">
							 	<div class="form-group"> 
									<label class="col-sm-2 control-label" for="inputEmail3">Start Date</label> 
									<div class="col-sm-10"> 
										<div id="year-view" class="input-group date"> 
											<input id="start_date" name="startdate" type="text" class="form-control" required=""> 
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
										</div>
									 </div> 
								</div> 
								<div class="form-group"> 
									 <label class="col-sm-2 control-label" for="inputPassword3">End Date</label> 
									 <div class="col-sm-10"> 
									   <div id="year-view2" class="input-group date"> 
											<input id="end_date" name="enddate" type="text" class="form-control" required=""> 
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
										</div>
									 </div> 
								</div> 								
								<div class="form-group"> 
									 <label class="col-sm-2 control-label" for="inputPassword3">Account Type</label> 
									 <div class="col-sm-10"> 
									   <select class="select2-placeholer form-control" name="accttype" data-placeholder="Select a state">
									   		<option value="">Select a state</option>
								   			<option value="1">CURRENT ACCOUNT</option>
								   			<option value="2">SAVINGS ACCOUNT</option>
										</select>
									 </div> 
								</div> 
								<div class="form-group"> 
									<div class="col-sm-offset-2 col-sm-10"> 
										<button class="btn btn-primary pull-right" type="submit">SEARCH</button> 
									</div> 
								</div> 
							</form>
						</div>
						<div class="col-md-6">
							{!! $chartjs->render() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

 <div class="row">
		<div class="col-lg-12 animatedParent animateOnce z-index-50">
			<div class="panel panel-default animated fadeInUp">
				
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover dataTables-example" >
							<thead>
								<tr>
									<th>BBAN</th>
									<th>ACCOUNT NAME</th>
									<th>ACCOUNT TYPE</th>
									<th>ACCOUNT STATUS</th>
									<th>DATE OPENED</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($mbank_stmts as $mbank_stmt)
									<tr class="gradeX">
										<td>{{$mbank_stmt->acct_link}}</td>
										<td>{{$mbank_stmt->acct_descrp}}</td>
										<td>{{$mbank_stmt->prod_descrp}}</td>
										<td>{{$mbank_stmt->status_indicator}}</td>
										<td>{{$mbank_stmt->date_opened}}</td>
									</tr>
								@endforeach										
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')
<script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{asset('js/plugins/datatables/extensions/Buttons/js/buttons.html5.js')}}"></script>
<script src="{{asset('js/plugins/datatables/extensions/Buttons/js/buttons.colVis.js')}}"></script>
<script>
	$(document).ready(function () {
		$('.dataTables-example').DataTable({
			dom: '<"html5buttons" B>lTfgitp',
			buttons: [
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns: [ 0, ':visible' ]
					}
				},
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: ':visible'
					}
				},
				{
					extend: 'pdfHtml5',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4 ]
					}
				},
				'colvis'
			]
		});

		$('#year-view').datepicker({
			startView: 2,
			keyboardNavigation: false,
			forceParse: false,
			format: "yyyy-m-dd",
			autoclose:true
		});

		$('#year-view2').datepicker({
			startView: 2,
			keyboardNavigation: false,
			forceParse: false,
			format: "yyyy-m-dd",
			autoclose:true
		});

		// $("#year-view").prop("readonly", true);
		// $('#start_date').prop('disabled', true);
		// $('#end_date').prop('disabled', true);

		$(".select2-placeholer").select2({
			allowClear: true
		});

	});
</script>

	<script type="text/javascript">
	    $('#mbank').addClass('active');
	    $('#mb-acct').addClass('active');
	    $('#mbank-collapse').addClass('in');
	</script>
@endsection