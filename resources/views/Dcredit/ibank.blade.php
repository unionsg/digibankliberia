@extends('layouts.master')

@section('styles')
<link href="{{asset('css/plugins/datatables/jquery.dataTables.css')}}" rel="stylesheet">
<link href="{{asset('js/plugins/datatables/extensions/Buttons/css/buttons.dataTables.css')}}" rel="stylesheet">

@endsection

@section('title')
 Internet Banking ACH
@endsection

@section('header')
  <h1 class="page-title">Internet Banking ACH</h1>
@endsection

@section('content')

<div class="row">
		<div class="col-lg-12 animatedParent animateOnce z-index-50">
			<div class="panel panel-default animated fadeInUp">				
				<div class="panel-body">
					<div class="row col-with-divider">
						<div class="col-md-6">
							<form class="form-horizontal" method="POST" action="ibank-achsrch"> 
								<input type="hidden" name="_token" value="{{csrf_token()}}">
							 	<div class="form-group"> 
									<label class="col-sm-2 control-label" for="inputEmail3">Start Date</label> 
									<div class="col-sm-10"> 
										<div id="year-view" class="input-group date"> 
											<input id="start_date" name="startdate" type="text" class="form-control" required=""> 
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
										</div>
									 </div> 
								</div> 
								<div class="form-group"> 
									 <label class="col-sm-2 control-label" for="inputPassword3">End Date</label> 
									 <div class="col-sm-10"> 
									   <div id="year-view2" class="input-group date"> 
											<input id="end_date" name="enddate" type="text" class="form-control" required=""> 
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
										</div>
									 </div> 
								</div> 	
								<div class="form-group"> 
									 <label class="col-sm-2 control-label" for="inputPassword3">Status</label> 
									 <div class="col-sm-10"> 
									   <select class="select2-placeholer form-control" name="status" data-placeholder="Select Status">
									   		<option value="">Select a state</option>								
											<option value="P">PENDING SETTLEMENT</option>
											<option value="A">SETTLED</option>
											<option value="V">REVERSED</option>
											<option value="O">PENDING APPROVAL</option>
										</select>
									 </div> 
								</div> 
								<div class="form-group"> 
									 <label class="col-sm-2 control-label" for="inputPassword3">Bank</label> 
									 <div class="col-sm-10"> 
									   <select class="select2-placeholer form-control" name="bank" data-placeholder="Select Status">	
									   		<option value="">Select a state</option>	
									   		@foreach ($banks as $bank)
											<option value="{{$bank->description}}">{{$bank->description}}</option>
											@endforeach
										</select>
									 </div> 
								</div> 							
								<div class="form-group"> 
									<div class="col-sm-offset-2 col-sm-10"> 
										<button class="btn btn-primary pull-right" type="submit">SEARCH</button> 
									</div> 
								</div> 
							</form>
						</div>
						<div class="col-md-6">
							{!! $chartjs->render() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

 <div class="row">
				<div class="col-lg-12 animatedParent animateOnce z-index-50">
					<div class="panel panel-default animated fadeInUp">						
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover dataTables-example" >
									<thead>
										<tr>
											<th>DEBIT ACCOUNT</th>
											<th>CREDIT ACCOUNT</th>
											<th>CREDIT BANK</th>
											<th>CURRENCY</th>
											<th>AMOUNT</th>
											<th>VALUE DATE</th>
											<th>POSTING DATE</th>
											<th>STATUS</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($ibank_trans as $ibank_tran)
											<tr class="gradeX">
												<td><a data-toggle="popover" title="BBAN" data-placement="top" data-html="true" data-content="{{$ibank_tran->debit_acct}} </br>">{{$ibank_tran->debit_name}}</a></td>
												<td>{{$ibank_tran->credit_acct}}</td>
												<td>{{$ibank_tran->credit_bank}}</td>
												<td>{{$ibank_tran->currency}}</td>
												<td style="text-align: right;">{{number_format($ibank_tran->amount,2, '.', ',')}}</td>
												<td>{{$ibank_tran->posting_date}}</td>
												<td>{{$ibank_tran->posting_sys_date}}</td>
												@if($ibank_tran->flag=='P')
												<td><button class="btn btn-xs btn-primary">PENDING SETTLEMENT</button></td>
												@elseif($ibank_tran->flag=='A')
												<td><button class="btn btn-xs btn-success">SETTLED</button></td>
												@elseif($ibank_tran->flag=='V')
												<td><button class="btn btn-xs btn-danger">REVERSED</button></td>
												@elseif($ibank_tran->flag=='O' or $ibank_tran->flag=='Y')
												<td><button class="btn btn-xs btn-warning">PENDING APPROVAL</button></td>
												@endif
											</tr>
										@endforeach										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
@endsection

@section('scripts')
<script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{asset('js/plugins/datatables/extensions/Buttons/js/buttons.html5.js')}}"></script>
<script src="{{asset('js/plugins/datatables/extensions/Buttons/js/buttons.colVis.js')}}"></script>
<script>
	$(document).ready(function () {
		$('.dataTables-example').DataTable({
			dom: '<"html5buttons" B>lTfgitp',
			buttons: [
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns: [ 0, ':visible' ]
					}
				},
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: ':visible'
					}
				},
				{
					extend: 'pdfHtml5',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4 ]
					}
				},
				'colvis'
			]
		});

		$('#year-view').datepicker({
			startView: 2,
			keyboardNavigation: false,
			forceParse: false,
			format: "yyyy-m-dd",
			autoclose:true
		});

		$('#year-view2').datepicker({
			startView: 2,
			keyboardNavigation: false,
			forceParse: false,
			format: "yyyy-m-dd",
			autoclose:true
		});
		

		$(".select2-placeholer").select2({
			allowClear: true
		});
	});
</script>

	<script type="text/javascript">
	    $('#ibank').addClass('active');
	    $('#ib-dcredit').addClass('active');
	    $('#ibank-collapse').addClass('in');
	</script>
@endsection