@extends('layouts.app')

@section('content')

<div class="login-content">
    <h2>Forgot your password?</h2>
    <p>Don't worry, we'll send you an email to reset your password.</p>

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">           
            <input id="email" type="email" class="form-control" name="email" placeholder="Your Email" value="{{ old('email') }}" required>
            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif           
        </div>
         <p>Don't remember your email? <a href="#">Contact Support</a>.</p>             
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">
                Send Password Reset Link
            </button>
        </div>
    </form>
</div>
@endsection
