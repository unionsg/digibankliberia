@extends('layouts.app')

@section('content')
<div class="login-content">
    <h2><strong>Welcome</strong>, please login</h2>
   <form class="form-horizontal" method="POST" action="{{ route('login') }}">
       {{ csrf_field() }}

       <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
           <input id="email" type="email" class="form-control" placeholder="E-Mail Address" name="email" value="{{ old('email') }}" required autofocus>

           @if ($errors->has('email'))
               <span class="help-block">
                   <strong>{{ $errors->first('email') }}</strong>
               </span>
           @endif
       </div>

       <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
               <input id="password" type="password" placeholder="Password" class="form-control" name="password" required>

               @if ($errors->has('password'))
                   <span class="help-block">
                       <strong>{{ $errors->first('password') }}</strong>
                   </span>
               @endif
       </div>

       <div class="form-group">
         <div class="checkbox checkbox-replace">
            <input type="checkbox" id="remeber" name="remember" {{ old('remember') ? 'checked' : '' }}>
            <label for="remeber">Remember me</label>
          </div>
       </div>

      
      <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block">Login</button>
      </div>

      <p class="text-center"><a href="{{ route('password.request') }}">Forgot your password?</a></p>         

   </form>
</div>
@endsection
