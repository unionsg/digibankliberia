@extends('layouts.master')

@section('styles')
<link href="{{asset('css/plugins/datatables/jquery.dataTables.css')}}" rel="stylesheet">
<link href="{{asset('js/plugins/datatables/extensions/Buttons/css/buttons.dataTables.css')}}" rel="stylesheet">

@endsection

@section('title')
 Blocked Users
@endsection

@section('header')
  <h1 class="page-title">Blocked Users</h1>
@endsection

@section('content')

 <div class="row">
				<div class="col-lg-12 animatedParent animateOnce z-index-50">
					<div class="panel panel-default animated fadeInUp">
						<!-- <div class="panel-heading clearfix">
							<h3 class="panel-title">Basic Data Tables with responsive Plugin</h3>
							<ul class="panel-tool-options"> 
								<li><a data-rel="collapse" href="#"><i class="icon-down-open"></i></a></li>
								<li><a data-rel="reload" href="#"><i class="icon-arrows-ccw"></i></a></li>
								<li><a data-rel="close" href="#"><i class="icon-cancel"></i></a></li>
							</ul>
						</div> -->
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover dataTables-example" >
									<thead>
										<tr>
											<th>ACCT NUM</th>
											<th>NAME</th>
											<th>USER ID</th>
											<th>USER ALIAS</th>
											<th>EMAIL</th>
											<th>TELEPHONE</th>
											<th>STATUS</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($lkusers as $lkuser)
											<tr class="gradeX">
												<td>{{$lkuser->customer_number}}</td>
												<td>{{$lkuser->customer_name}}</td>
												<td>{{$lkuser->user_id}}</td>
												<td>{{$lkuser->user_alias}}</td>
												<td>{{$lkuser->email}}</td>
												<td>{{$lkuser->telephone}}</td>
												<td><button class="btn btn-xs btn-danger">BLOCKED</button></td>
											</tr>
										@endforeach										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

@endsection

@section('scripts')
<script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{asset('js/plugins/datatables/extensions/Buttons/js/buttons.html5.js')}}"></script>
<script src="{{asset('js/plugins/datatables/extensions/Buttons/js/buttons.colVis.js')}}"></script>
<script>
	$(document).ready(function () {
		$('.dataTables-example').DataTable({
			dom: '<"html5buttons" B>lTfgitp',
			buttons: [
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns: [ 0, ':visible' ]
					}
				},
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: ':visible'
					}
				},
				{
					extend: 'pdfHtml5',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4 ]
					}
				},
				'colvis'
			]
		});
	});
</script>

	<script type="text/javascript">
	    $('#users').addClass('active');
	    $('#lck-users').addClass('active');
	    $('#users-collapse').addClass('in');
	</script>
@endsection