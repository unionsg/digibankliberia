@extends('layouts.master')

@section('styles')
<link href="{{asset('css/plugins/datatables/jquery.dataTables.css')}}" rel="stylesheet">
<link href="{{asset('js/plugins/datatables/extensions/Buttons/css/buttons.dataTables.css')}}" rel="stylesheet">

@endsection

@section('title')
 Blocked Users
@endsection

@section('header')
  <h1 class="page-title">Active/Inactive Users</h1>
@endsection

@section('content')

 <div class="row">
		<div class="col-lg-12 animatedParent animateOnce z-index-50">
			<div class="tabs-container animated fadeInUp">
				<ul class="nav nav-tabs">
					<li class="active"><a aria-expanded="true" href="#home" data-toggle="tab">Active</a></li>
					<li><a aria-expanded="false" href="#profile" data-toggle="tab">Inactive</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="home">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover dataTables-example" >
									<thead>
										<tr>
											<th>ACCT NUM</th>
											<th>NAME</th>
											<th>USER ID</th>
											<th>USER ALIAS</th>
											<th>EMAIL</th>
											<th>TELEPHONE</th>
											<th>STATUS</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($activeusers as $activeuser)
											<tr class="gradeX">
												<td>{{$activeuser->customer_number}}</td>
												<td>{{$activeuser->customer_name}}</td>
												<td>{{$activeuser->user_id}}</td>
												<td>{{$activeuser->user_alias}}</td>
												<td>{{$activeuser->email}}</td>
												<td>{{$activeuser->telephone}}</td>
												<td><button class="btn btn-xs btn-success">ACTIVE</button></td>
											</tr>
										@endforeach										
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="profile">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover dataTables-example" >
									<thead>
										<tr>
											<th>ACCT NUM</th>
											<th>NAME</th>
											<th>USER ID</th>
											<th>USER ALIAS</th>
											<th>EMAIL</th>
											<th>TELEPHONE</th>
											<th>STATUS</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($inactiveusers as $inactiveuser)
											<tr class="gradeX">
												<td>{{$inactiveuser->customer_number}}</td>
												<td>{{$inactiveuser->customer_name}}</td>
												<td>{{$inactiveuser->user_id}}</td>
												<td>{{$inactiveuser->user_alias}}</td>
												<td>{{$inactiveuser->email}}</td>
												<td>{{$inactiveuser->telephone}}</td>
												<td><button class="btn btn-xs btn-danger">INACTIVE</button></td>
											</tr>
										@endforeach										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')
<script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{asset('js/plugins/datatables/extensions/Buttons/js/buttons.html5.js')}}"></script>
<script src="{{asset('js/plugins/datatables/extensions/Buttons/js/buttons.colVis.js')}}"></script>
<script>
	$(document).ready(function () {
		$('.dataTables-example').DataTable({
			dom: '<"html5buttons" B>lTfgitp',
			buttons: [
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns: [ 0, ':visible' ]
					}
				},
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: ':visible'
					}
				},
				{
					extend: 'pdfHtml5',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4 ]
					}
				},
				'colvis'
			]
		});
	});
</script>

	<script type="text/javascript">
	    $('#users').addClass('active');
	    $('#act-users').addClass('active');
	    $('#users-collapse').addClass('in');
	</script>
@endsection