@extends('layouts.master')

@section('styles')
<link href="{{asset('css/plugins/datatables/jquery.dataTables.css')}}" rel="stylesheet">
<link href="{{asset('js/plugins/datatables/extensions/Buttons/css/buttons.dataTables.css')}}" rel="stylesheet">

@endsection

@section('title')
 Internet Banking - Salone link
@endsection

@section('header')
  <h1 class="page-title">Internet Banking - Salone link</h1>
@endsection

@section('content')
<div class="row">
		<div class="col-lg-12 animatedParent animateOnce z-index-50">
			<div class="panel panel-default animated fadeInUp">				
				<div class="panel-body">
					<div class="row col-with-divider">
						<div class="col-md-6">
							<form class="form-horizontal" method="POST" action="{{url('ibank-slinksrch')}}"> 
								<input type="hidden" name="_token" value="{{csrf_token()}}">
							 	<div class="form-group"> 
									<label class="col-sm-2 control-label" for="inputEmail3">Start Date</label> 
									<div class="col-sm-10"> 
										<div id="year-view" class="input-group date"> 
											<input id="start_date" name="startdate" type="text" class="form-control" required=""> 
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
										</div>
									 </div> 
								</div> 
								<div class="form-group"> 
									 <label class="col-sm-2 control-label" for="inputPassword3">End Date</label> 
									 <div class="col-sm-10"> 
									   <div id="year-view2" class="input-group date"> 
											<input id="end_date" name="enddate" type="text" class="form-control" required=""> 
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
										</div>
									 </div> 
								</div>
								<div class="form-group"> 
									 <label class="col-sm-2 control-label" for="inputPassword3">Status</label> 
									 <div class="col-sm-10"> 
									   <select class="select2-placeholer form-control" name="status" data-placeholder="Select Status">
									   		<option value="">Select a status</option>								
											<option value="P">SETTLED</option>
											<option value="A">NOT SETTLED</option>
										</select>
									 </div> 
								</div>  								
								<div class="form-group"> 
									<div class="col-sm-offset-2 col-sm-10"> 
										<button class="btn btn-primary pull-right" type="submit">SEARCH</button> 
									</div> 
								</div> 
							</form>
						</div>
						<div class="col-md-6">
							{!! $chartjs->render() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

 <div class="row">
	<div class="col-lg-12 animatedParent animateOnce z-index-50">
		<div class="panel panel-default animated fadeInUp">						
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover dataTables-example" >
						<thead>
							<tr>
								<th>DEBIT ACCOUNT</th>
								<th>CURRENCY</th>
								<th>AMOUNT</th>
								<th>CHARGE</th>											
								<th>BENE. TELEPHONE</th>
								<th>POSTING DATE</th>
								<th>STATUS</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($ibank_slinktrans as $ibank_slinktran)
								<tr class="gradeX">
									<td><a data-toggle="popover" title="BBAN" data-placement="top" data-html="true" data-content="{{$ibank_slinktran->debit_acct}} </br>">{{$ibank_slinktran->debit_acct}}</a></td>
									<td>{{$ibank_slinktran->currency}}</td>
									<td style="text-align: right;">{{$ibank_slinktran->amount}}</td>
									<td style="text-align: right;">{{$ibank_slinktran->charge}}</td>	
									<td>{{$ibank_slinktran->bene_tel}}</td>
									<td>{{$ibank_slinktran->posting_date}}</td>
									@if($ibank_slinktran->flag=='A')
									<td><button class="btn btn-xs btn-primary">NOT SETTLED</button></td>
									@elseif($ibank_slinktran->flag=='P')
									<td><button type="button" class="btn btn-xs btn-success" data-toggle="popover" title="REDEEM DETAILS" data-placement="top" data-html="true" data-content="<b>PAID OUT BY :</b> </br> {{$ibank_slinktran->paid_by}} </br> <b>DATE :</b> </br>{{$ibank_slinktran->paid_date}}">SETTLED</button></td>
									@endif
								</tr>
							@endforeach										
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/extensions/Buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/jszip.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/pdfmake.min.js')}}"></script>
<script src="{{asset('js/plugins/datatables/vfs_fonts.js')}}"></script>
<script src="{{asset('js/plugins/datatables/extensions/Buttons/js/buttons.html5.js')}}"></script>
<script src="{{asset('js/plugins/datatables/extensions/Buttons/js/buttons.colVis.js')}}"></script>
<script>
	$(document).ready(function () {
		$('.dataTables-example').DataTable({
			dom: '<"html5buttons" B>lTfgitp',
			buttons: [
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns: [ 0, ':visible' ]
					}
				},
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: ':visible'
					}
				},
				{
					extend: 'pdfHtml5',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4 ]
					}
				},
				'colvis'
			]
		});

		$('#year-view').datepicker({
			startView: 2,
			keyboardNavigation: false,
			forceParse: false,
			format: "yyyy-m-dd",
			autoclose:true
		});

		$('#year-view2').datepicker({
			startView: 2,
			keyboardNavigation: false,
			forceParse: false,
			format: "yyyy-m-dd",
			autoclose:true
		});
		

		$(".select2-placeholer").select2({
			allowClear: true
		});
	});
</script>

	<script type="text/javascript">
	    $('#ibank').addClass('active');
	    $('#ib-ftrans').addClass('active');
	    $('#ibank-slink').addClass('active');
	    $('#ibank-collapse').addClass('in');
	    $('#ibank-sub-collapse').addClass('in');
	</script>
@endsection