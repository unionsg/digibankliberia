<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');



Route::get('/','HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/pdf','HomeController@export_pdf');

// user profile
Route::get('/lk-users','UserprofileController@locked_users')->name('lkusers');
Route::get('/blk-users','UserprofileController@blocked_users')->name('blkusers');
Route::get('/reg-users','UserprofileController@registered_users')->name('regusers');
Route::post('reg-users','UserprofileController@registered_srch')->name('reguserssearch');
Route::get('/deactv-users','UserprofileController@deactivated_users')->name('deactvusers');
Route::get('/active-users','UserprofileController@active_users')->name('deactvusers');


// direct credit
Route::get('/mbank-ach','DirectcreditController@mbank_trans')->name('mbankach');
Route::get('/ibank-ach','DirectcreditController@ibank_trans')->name('ibankach');
Route::get('/cbank-ach','DirectcreditController@cbank_trans')->name('cbankach');
Route::post('mbank-achsrch','DirectcreditController@mbank_search')->name('mbankachsrch');
Route::post('ibank-achsrch','DirectcreditController@ibank_search')->name('ibankachsrch');
Route::post('cbank-achsrch','DirectcreditController@cbank_search')->name('cbankachsrch');


//statement request
Route::get('/mbank-stmt','StatementController@mbank_trans')->name('mbankstmt');
Route::get('/ibank-stmt','StatementController@ibank_trans')->name('ibankstmt');
Route::get('/cbank-stmt','StatementController@cbank_trans')->name('cbankstmt');
Route::post('mbank-stmtsrch','StatementController@mbank_search')->name('mbankstmtsrch');
Route::post('ibank-stmtsrch','StatementController@ibank_search')->name('ibankstmtsrch');
Route::post('cbank-stmtsrch','StatementController@cbank_search')->name('cbankstmtsrch');


//chequebook request
Route::get('/mbank-chqbk','ChequebookController@mbank_trans')->name('mbankchqbk');
Route::get('/ibank-chqbk','ChequebookController@ibank_trans')->name('ibankchqbk');
Route::get('/cbank-chqbk','ChequebookController@cbank_trans')->name('cbankchqbk');
Route::post('mbank-chqsrch','ChequebookController@mbank_search')->name('mbankchqsrch');
Route::post('ibank-chqsrch','ChequebookController@ibank_search')->name('ibankchqsrch');
Route::post('cbank-chqsrch','ChequebookController@cbank_search')->name('cbankchqsrch');

//funds transfer
Route::get('/mbank-int-transfer','FundstransferController@mbank_intertrans')->name('mbankinttransfer');
Route::get('/mbank-qr-transfer','FundstransferController@mbank_qrtrans')->name('mbankqrtransfer');
Route::get('/mbank-slink','FundstransferController@mbank_slinktrans')->name('mbankslink');
Route::post('mbank-slinksrch','FundstransferController@mbank_slinksearch')->name('mbankslinksrch');
Route::post('mbank-int-transfersrch','FundstransferController@mbank_intertranssearch')->name('mbankinttransfersrch');
Route::post('mbank-qr-transfersrch','FundstransferController@mbank_qrsearch')->name('mbankqrtransfersrch');

Route::get('/ibank-int-transfer','FundstransferController@ibank_intertrans')->name('ibankinttransfer');
Route::get('/ibank-qr-transfer','FundstransferController@ibank_qrtrans')->name('ibankqrtransfer');
Route::get('/ibank-slink','FundstransferController@ibank_slinktrans')->name('ibankslink');
Route::post('ibank-slinksrch','FundstransferController@ibank_slinksearch')->name('ibankslinksrch');
Route::post('ibank-int-transfersrch','FundstransferController@ibank_intertranssearch')->name('ibankinttransfersrch');
Route::post('ibank-qr-transfersrch','FundstransferController@ibank_qrsearch')->name('ibankqrtransfersrch');

Route::get('/cbank-int-transfer','FundstransferController@cbank_intertrans')->name('cbankinttransfer');
Route::get('/cbank-qr-transfer','FundstransferController@cbank_qrtrans')->name('cbankqrtransfer');
Route::get('/cbank-slink','FundstransferController@cbank_slinktrans')->name('cbankslink');
Route::post('cbank-slinksrch','FundstransferController@cbank_slinksearch')->name('cbankslinksrch');
Route::post('cbank-int-transfersrch','FundstransferController@cbank_intertranssearch')->name('cbankinttransfersrch');
Route::post('cbank-qr-transfersrch','FundstransferController@cbank_qrsearch')->name('cbankqrtransfersrch');

//login summary
Route::get('/mbank-loginsum','LoginstatController@mbankindex')->name('mbankloginsum');
Route::post('mbank-loginsumsrch','LoginstatController@mbanksearch')->name('mbankloginsumsrch');
Route::get('/ibank-loginsum','LoginstatController@ibankindex')->name('ibankloginsum');
Route::post('ibank-loginsumsrch','LoginstatController@ibanksearch')->name('ibankloginsumsrch');
Route::get('/cbank-loginsum','LoginstatController@cbankindex')->name('cbankloginsum');
Route::post('cbank-loginsumsrch','LoginstatController@cbanksearch')->name('cbankloginsumsrch');

//login log details
Route::get('/mbank-logindetails','LoginstatController@mbankdetailindex')->name('mbanklogindetails');
Route::post('mbank-logindetailssrch','LoginstatController@mbankdetailsearch')->name('mbanklogindetailssrch');
Route::get('/ibank-logindetails','LoginstatController@ibankdetailindex')->name('ibanklogindetails');
Route::post('ibank-logindetailssrch','LoginstatController@ibankdetailsearch')->name('ibanklogindetailssrch');
Route::get('/cbank-logindetails','LoginstatController@cbankdetailindex')->name('cbanklogindetails');
Route::post('cbank-logindetailssrch','LoginstatController@cbankdetailsearch')->name('cbanklogindetailssrch');

//Revenue
Route::get('/mbank-revenue','RevenueController@mbankindex')->name('mbankrevenue');
Route::post('mbank-revenuerch','RevenueController@mbanksearch')->name('mbankrevenuesrch');
Route::get('/ibank-revenue','RevenueController@ibankindex')->name('ibankrevenue');
Route::post('ibank-revenuerch','RevenueController@ibanksearch')->name('ibankrevenuesrch');
Route::get('/mteller-revenue','RevenueController@mtellerindex')->name('mtellerrevenue');
Route::post('mteller-revenuerch','RevenueController@mtellersearch')->name('mtellerrevenuesrch');
Route::get('/cbank-revenue','RevenueController@cbankindex')->name('cbankrevenue');
Route::post('cbank-revenuerch','RevenueController@cbanksearch')->name('cbankrevenuesrch');

//Audit
Route::get('/mbank-audit','AuditController@mbankindex')->name('mbankaudit');
Route::post('mbank-auditsrch','auditController@mbanksearch')->name('mbankauditsrch');
Route::get('/ibank-audit','auditController@ibankindex')->name('ibankaudit');
Route::post('ibank-auditsrch','auditController@ibanksearch')->name('ibankauditsrch');

//statement request
Route::get('/mbank-acct','AccountController@mbank_trans')->name('mbankacct');
Route::get('/ibank-acct','AccountController@ibank_trans')->name('ibankacct');
Route::post('mbank-acctsrch','AccountController@mbank_search')->name('mbankacctsrch');
Route::post('ibank-acctsrch','AccountController@ibank_search')->name('ibankacctsrch');